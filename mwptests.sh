RED='\033[0;31m'
GREEN='\033[0;32m'
for f in ./mytest/infinite_*.c
do
  ./ccomp -ssa mwp $f &> out.log
	if cat out.log | grep -q fail; then 
    echo -e "${RED}$f FAIL!"
    exit 0
	fi
	if cat out.log | grep -q infinite; then 
    echo -e "${GREEN}$f OK!"
  else
    echo -e "${RED}$f KO!"
	fi
  rm out.log
done

for f in ./mytest/notinfinite_*.c
do
  ./ccomp -ssa mwp $f &> out.log
	if cat out.log | grep -q fail; then 
    echo -e "${RED}$f FAIL!"
    exit 0
	fi
	if cat out.log | grep -q infinite; then 
    echo -e "${RED}$f KO!"
  else
    echo -e "${GREEN}$f OK!"
	fi
  rm out.log
done
