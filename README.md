# CompCert
The verified C compiler.

## Overview
The CompCert C verified compiler is a compiler for a large subset of the
C programming language that generates code for the PowerPC, ARM, x86 and
RISC-V processors.

The distinguishing feature of CompCert is that it has been formally
verified using the Coq proof assistant: the generated assembly code is
formally guaranteed to behave as prescribed by the semantics of the
source C code.

For more information on CompCert (supported platforms, supported C
features, installation instructions, using the compiler, etc), please
refer to the [Web site](http://compcert.inria.fr/) and especially
the [user's manual](http://compcert.inria.fr/man/).

## CompCertSSA version

This development is a version of CompCert extended with an SSA
middle-end:

- construction of the SSA form, from RTL
- SSA-based optimizations
- SSA-related librairies on dominance and dominators
- SSA destruction with coalescing, on the Conventional SSA form

The following people contibuted to this extension (alphabetic order):
Sandrine Blazy, Delphine Demange, Yon Fernandez de Retana, David
Pichardie, Léo Stefanesco.

## License
CompCert is not free software.  This non-commercial release can only
be used for evaluation, research, educational and personal purposes.
A commercial version of CompCert, without this restriction and with
professional support, can be purchased from
[AbsInt](https://www.absint.com).  See the file `LICENSE` for more
information.

## Copyright
The CompCert verified compiler is Copyright Institut National de
Recherche en Informatique et en Automatique (INRIA) and 
AbsInt Angewandte Informatik GmbH.

The additions related to the SSA middle-end are Copyright Univ Rennes,
Inria, IRISA.

## Contact
General discussions on CompCert take place on the
[compcert-users@inria.fr](https://sympa.inria.fr/sympa/info/compcert-users)
mailing list.

For inquiries on the commercial version of CompCert, please contact
info@absint.com

For inquiries on the SSA-specific additions, please contact Delphine
Demange.

### Install

- You'll need `opam` and some tools easily installable using it.
  Here is my opam-switch to build CompcertSSA :

package | version | description
--- | - | -------
conf-autoconf|0.1|Virtual package relying on autoconf installation
coq|8.11.2| Formal proof management system
dune|2.8.4|Fast, portable and opinionated build system
menhir|20210310| An LR(1) parser generator
merlin|3.4.2|Editor helper, provides completion, typing and source browsing in Vim and Emacs
ocaml|4.10.0| The OCaml compiler (virtual package)
ocaml-base-compiler| 4.10.0 | Official 4.05.0 release
ocaml-config| 1| OCaml Switch Configuration
ocamlbuild| 0.14.0| OCamlbuild is a build system with builtin rules to easily build most OCaml projects.
ocamlfind| 1.9.1|A library manager for OCaml
ocamlgraph| 2.0.0|A generic graph library for OCaml

If you need something else, the `./configure …` will tell you. `opam
install …` should work.

- All explanations are [here](http://compcertssa.gforge.inria.fr/)
  `./configure x86_64-linux` for 64bits linux systems.
  `make depends; make all`

### Run MWP Analysis

To run on
```
./ccomp path/to/file.c -ssa mwp -dssa

```

option `-ssa on` enables SSA representation and `-ssa mwp` run mwp
analysis over SSA representation and print out the relations.

### Run MWP test

```
make mwp_test

```
