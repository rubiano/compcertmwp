open Maps
open Hashtbl
open Camlcoq
open BinNums
open MWPUtils

module type S =
  sig
    type monomial
    type deltaG
    type n_deltagraphs
    type label

    val addEdge: n_deltagraphs -> monomial -> monomial -> label -> n_deltagraphs
    val insert_monomial: n_deltagraphs -> monomial -> n_deltagraphs
    val isfull: deltaG -> monomial -> positive -> int -> bool
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val printDeltaGraph: n_deltagraphs -> unit
    val new_ndg: n_deltagraphs
  end

module Make(Mono: MonomialBase.S): (S with type monomial = Mono.t) = struct
  type monomial = Mono.t
  type label = positive
  open Mono

  (* type monomial = Monomial_MWP.t *)
  (* open Monomial_MWP *)

  (* DeltaG is a dictionary representing a weighted graph *)
  (* of tuple of deltas (also referenced here as monomial_list *)
  (* aka: a monomial without its scalar). *)

  (* We use tuple here instead of list because we want them to be *)
  (* hashable (as key in dictionary). *)

  (* We will often refere to tuple of deltas as simple node. *)
  (* But a node with lenght ! *)

  (* Nodes are "sorted" by this lenght in order to be compared by chunks of *)
  (* same size. *)

  (* Weight of edge represents the index where the nodes differ. *)

  (* example: *)

  (* ``` *)
  (*                              ↓ *)
  (*     n1 = ( (0,1) , (0,2) , (0,3), (0,4) ) *)
  (*     n2 = ( (0,1) , (0,2) , (1,3), (0,4) ) *)
  (* ``` *)

  (* in our graph will have: *)

  (* ``` *)
  (*     n1 <---- 3 ----> n2 *)
  (* ``` *)

  (* or *)

  (* ```python *)
  (* size = 4   ↓ *)

  (* graph_dict[4][n1][n2] = 3 *)
  (* ``` *)

  (* Note: yes it also means it's symetric : *)

  (* ```python *)
  (* graph_dict[4][n2][n1] = 3 *)
  (* ``` *)

  (* This representation will help us simplify the evaluation by *)
  (* removing redundant/irrelevant choices/paths. *)

  (* Interface of monomials for DeltaG *)
  module IMonomial =
   struct
     (* couple of monomial where order does not matter (should be a set ?) *)
    type t = monomial * monomial

    (* Not sure of that … *)
    let index (m1,m2) = P.of_int (hash (get_deltas m1) * hash (get_deltas m2))

    (** val eq : monomial -> monomial -> bool **)
    let eq (m11,m12) (m21,m22) = 
      if equal m11 m21 then equal m12 m22 else
        if equal m11 m22 then equal m12 m21 else false
   end

  module DeltaG = ITree(IMonomial)

  type deltaG = positive DeltaG.t * monomial list * IMonomial.t list
  type n_deltagraphs = deltaG PTree.t

  let new_ndg = PTree.empty

  let printDeltaGraph (ngraph:n_deltagraphs) =
    Printf.eprintf "********\n";
    let dgraphs = PTree.elements ngraph in
    List.iter (fun (n,g) -> 
      let (dg,ml,pairs) = g in
      Printf.eprintf "%d : " (P.to_int n);
      List.iter (fun m -> Printf.eprintf "%s, " (Mono.to_str m)) ml;
      Printf.eprintf "\n Pairs :";
      List.iter (fun (m1,m2) -> 
        match DeltaG.get (m1,m2) dg with
        | None -> ()
        | Some i ->
        Printf.eprintf "(%s,%s:%d) " (Mono.to_str m1) (Mono.to_str m2) (P.to_int i)) pairs;
      Printf.eprintf "\n";
    ) dgraphs

  (* Compares two nodes
  Compares two lists of monomials (of the same lenth)
  and returns (diff, i) where diff is True if and only if
  both lists differ only on one element regarding to the same index
  and i is the index of the corresponding delta.
  Arguments:
      dl1: first monomial_list
      dl2: second monomial_list
      index: index with to check number of diff
  Returns:
      positive option : Some i
      i: the index of the delta which differs
      None if no diff matching
  *)
  let rec mono_diff (dl1:(delta_choice list)) (dl2:(delta_choice list)) (index:positive option) : positive option =
    match dl1 with
    | d::dl1' -> 
        if List.mem d dl2 
        (* continue without both same element *)
        then mono_diff dl1' (remove_first d dl2) index 
        else (match index with
        | Some i -> None (* already found one diff *)
        | None -> 
            (* let Delta(_,i) = d in *)
            let i = get_index d in
            (* continue *)
            mono_diff dl1' dl2 (Some i)
        )
    | [] -> match index with 
            | None -> None
            | Some id -> match dl2 with
                        | [d] -> 
                          (* let Delta(_,i) = d in *)
                          let i = get_index d in
                          if i = id then Some i
                          (* differ but not on same index *)
                          else None
                        | _ -> assert false


  (* Add an edge of label `label` btwn `node1` and `node2`
  If one node does not exists, it's created
  Symetry is also added in the graph
  Note:
      node2 should always exists if addEdge is always called
      from insert_monomial
  Arguments:
      node1: first node
      node2: second node
      label: label over the edge
  *)
  let addEdge (ngraph:n_deltagraphs) (node1:monomial) (node2:monomial) (label:positive) : n_deltagraphs =
    (* Printf.eprintf "AddEdge (%d:%s,%s:%d) !\n" (P.to_int (IMonomial.index (node1,node2))) (str_monomial node1) (str_monomial node2) (P.to_int label); *)
    (* addEdge is called only when len(node1) = len(node2) *)
    let size = List.length (get_deltas node1) in
    match PTree.get (P.of_int size) ngraph with
    | Some (dg,ml,el) -> 
        let nml = if List.mem node1 ml then ml else node1::ml in
        let value = (DeltaG.set (node1,node2) label dg, nml, (node1,node2)::el) in
        PTree.set (P.of_int size) value ngraph
    | None -> assert false (* should already added in insert_monomial *)

  (* Insert tuple in the graph
  it may exists cases where we need to perform simplification
  over monomials, this is why we added simplification boolean
  if monomial_list is already in the graph do nothing
  else compare it with all monomial_list of same size with
  mono_diff
  Note:
      Is it possible that we've already computed that diff in
      the other way ? (symetry)
      Answer:
          AFA it's called now, NO
  Arguments:
      monomial_list: tuple to insert in the graph
      simplification: boolean to inform if simplification is
      necessary
  *)
  let insert_monomial (ngraph:n_deltagraphs) (monom:monomial) : n_deltagraphs =
    (* Printf.eprintf "\tInsert %s \n" (str_monomial monom); *)
    let monomial_list = get_deltas monom in
    let n = List.length monomial_list in
    assert (n <> 0);
    match PTree.get (P.of_int n) ngraph with
    | None -> PTree.set (P.of_int n) (DeltaG.empty,[monom], []) ngraph
    | Some (dg,m_list,e_list) ->
        if List.mem monom m_list then
          ngraph
        else
          (* save the monom *)
          let ngraph = PTree.set (P.of_int n) (dg,monom::m_list,e_list) ngraph in
          List.fold_left (fun (acc) monomi -> 
            let somei = mono_diff monomial_list (get_deltas monomi) None in
            match somei with
            | Some i -> (addEdge acc monom monomi i)
            | None -> (acc)
          ) (ngraph) m_list

  let getIndexes (m:monomial) : positive list =
    let deltas = get_deltas m in
    List.map (fun d -> get_index d) deltas

  let getAllPairsWith (m:monomial) (g:deltaG) : IMonomial.t list =
    let (_,_,pairs) = g in
    List.filter (fun (a,b) -> ((equal a m) || (equal b m))) pairs

  (* let getNeighbors (m:monomial) (g:deltaG) : monomial list = *)
  (*   let pairs = getAllPairsWith m g in *)
  (*   List.map (fun (a,b) -> if equal a m then b else a) pairs *)

  let rec isfull' (dg: positive DeltaG.t) (index:positive) (i:int) (max_i:int) (pairs:IMonomial.t list) : bool =
    match pairs with
    | [] -> false
    | h::t -> 
        match DeltaG.get h dg with
        | None -> isfull' dg index i max_i t
        | Some id -> if id = index then (
            if (i+1) = (max_i-1) then true
            else isfull' dg index (i+1) max_i t
        ) else isfull' dg index i max_i t

  (*Check for cliques of same label
  example :
  m3 = ((0, 1), (2, 2), (0, 3))
  m4 = ((0, 1), (2, 2), (1, 3))
  m5 = ((0, 1), (2, 2), (2, 3))
  mono = m4
  index = 3
  max_choices = 3
  # m3 --3-- m4
  #   \\       |
  #    \\      |
  #     3     3
  #      \\    |
  #       \\   |
  #         m5
  return True
  Rq: transitivity here ! m3 -> m4 -> m5 => m3 -> m5
  Arguments:
      n: size of nodes or "level"
      mono: arround that node
      index: index with to find clique
  Returns:
      True if there is a clique
  *)
  let isfull (graph:deltaG) (mono:monomial) (index:positive) (max_choices:int) : bool =
    let pairs = getAllPairsWith mono graph in
    let (dg,_,_) = graph in
    isfull' dg index 0 max_choices pairs

  (* Remove delta with given index
  Arguments:
      ml: monomial_list as tuple
      index: index to remove
  Returns:
      a new tuple without detlas with index `index` *)
  let remove_index (m:monomial) (index:positive) : monomial =
    let s,deltas = get_product m in
    let deltas = List.filter (fun d -> let i = get_index d in i <> index) deltas in
    cons(s,deltas)

  let getOther (pair:monomial * monomial) (me:monomial) : monomial =
    let (m1,m2) = pair in
    if equal m1 me then m2 else m1

  (* Remove given tuple and neighbors connected with same label
    Removes also edge/labels connected to that node
    (which no longer exists) 
  *)
  let rec remove_monomial (m:monomial) (dg:deltaG) (index:positive) : deltaG =
    (* Printf.eprintf "Removing %s \n" (str_monomial m); *)
    let (g,ml,pairs) = dg in 
    let m_pairs = getAllPairsWith m dg in
    (* remove m from ml *)
    let ml' = List.filter (fun e -> e <> m) ml in
    (* remove all occurence of m in pairs *)
    (* let pairs' = List.filter (fun (a,b) -> not ((mono_eq a m) || (mono_eq b m))) pairs in *)
    let pairs' = List.filter (fun p -> not (List.mem p m_pairs)) pairs in
    (* remove all index in delta graph with m in pairs *)
    List.fold_left (fun (g,ml',pairs') p -> 
      match DeltaG.get p g with 
      | None -> (g,ml',pairs')
      | Some i ->
          if i = index then
            let g' = DeltaG.remove p g in
            remove_monomial (getOther p m) (g',ml',pairs') index
          else (g,ml',pairs')
    ) (g,ml',pairs') m_pairs

  (* Eliminate clique of same label in delta_graph
    example :
    ```
    m1 = ((0, 1), (0, 2))
    m2 = ((0, 1), (1, 2))
    m3 = ((0, 1), (2, 2), (0, 3))
    m4 = ((0, 1), (2, 2), (1, 3))
    m5 = ((0, 1), (2, 2), (2, 3))
    ```
    with list_of_max = [3,3,3,3]
    look for cliques of size 3 for each index
    delta graph :
    ```
     m1 --2-- m2
     m3 --3-- m4
       \\       |
        \\      |
         3     3
          \\    |
           \\   |
             m5
    ```
    will simplify to :
    ```
    m0 = ((0,1))
    ```
    Arguments:
        list_of_max: size of clique we want to eliminate regarding to index
        of deltas
    Returns:
        eliminates corresponding clique in the delta_graph, a boolean telling us if the deltagraph is infinite (there is no solution)
  *)
  let fusion (ngraph:n_deltagraphs ref) (max_i:int) : n_deltagraphs * bool =
    (* Start from longest monomial list to the shortest *)
    (* get delta graphs with size *)
    let graphs = PTree.elements !ngraph in
    (* sort delta graph by size *)
    let graphs = List.sort (fun (n1,_) (n2,_) -> (P.to_int n1) - (P.to_int n2)) graphs in
    (* reverse the list to have longest first *)
    let graphs = List.rev graphs in
    (* for delta graph of size n *)
    let (rndg,b) = List.fold_left (fun (ng_,i) (n,_) -> 
      (* Printf.eprintf "*** %d *** \n" (P.to_int n); *)
      let dg = match PTree.get n ng_ with
                | Some dg -> dg
                | None -> assert false
      in
      let (_,ml,_) = dg in
      (* For all monomials of size n : m *)
      List.fold_left (fun (ng,i) m -> 

        (* For all indexes in deltas of that monomial *)
        List.fold_left (fun (ng',i) index ->
          let dg' = match PTree.get n ng' with
                    | Some dg' -> dg'
                    | None -> assert false
          in
          let (_,ml',_) = dg' in 
          (* check if not already removed and if it's full *)
          if (List.mem m ml') && isfull dg' m index max_i then
            (* remove the monomial m from datagraph of size n *)
            let dg'' = remove_monomial m dg' index in
            let ng'' = PTree.set n dg'' ng' in
            (* if we just remove something of size 1 it means there is no solution *)
            if (P.to_int n) = 1 then (ng'', true) else
            (insert_monomial ng'' (remove_index m index), false)
          (* if already removed or no full go to next index *)
          else (ng',i)
        ) (ng,i) (getIndexes m)
      ) (ng_,i) ml
    ) (!ngraph,false) graphs in
    ngraph:= rndg;
    (rndg,b)
end

(*
(* Tests *) (* {↓{ *)
let test_mono_diff =
  let m1 = [Delta(1, (P.of_int 1)); Delta(1, (P.of_int 2))] in
  let m2 = [Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))] in
  let m3 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(3, (P.of_int 5))] in 
  let m4 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m5 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m6 = [Delta(1, (P.of_int 3)); Delta(3, (P.of_int 4)); Delta(3, (P.of_int 5))] in
  assert ((mono_diff m3 m4 None) = Some (P.of_int 5));
  assert ((mono_diff m4 m3 None) = Some (P.of_int 5));
  assert ((mono_diff m3 m6 None) = Some (P.of_int 4));
  assert ((mono_diff m6 m3 None) = Some (P.of_int 4));
  assert (mono_diff m1 m2 None = None);
  assert (mono_diff m2 m1 None = None);
  assert (mono_diff m3 m5 None = Some (P.of_int 5))

let test_insert_tuple =
  let m1 = [Delta(1, (P.of_int 1)); Delta(1, (P.of_int 2))] in
  let m2 = [Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))] in
  let m3 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(3, (P.of_int 5))] in 
  let m4 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m5 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m6 = [Delta(1, (P.of_int 3)); Delta(3, (P.of_int 4)); Delta(3, (P.of_int 5))] in

  let lm' = [m1;m2;m3;m4;m5;m6] in
  let lm = List.map (fun m -> Mono (SemiRing.I,m)) lm' in
  let m3' = List.nth lm 2 in
  let m4' = List.nth lm 3 in
  let m5' = List.nth lm 4 in
  assert (mono_eq m4' m5');
  let m6' = List.nth lm 5 in
  let ng = PTree.empty in
  let ng = List.fold_left (fun ng m -> insert_monomial ng m) ng lm in

  (* printDeltaGraph ng; *)

  assert ((mono_diff m3 m4 None) = Some (P.of_int 5));
  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m3',m4') dg = Some (P.of_int 5)
    | _ -> false);
  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m4',m3') dg = Some (P.of_int 5)
    | _ -> false);
  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> (match DeltaG.get (m3',m6') dg with
      | Some p -> p = (P.of_int 4)
      | None -> false
      )
    | _ -> false)

let test_remove_index=
  let m1 = Mono (SemiRing.I,[Delta(1, (P.of_int 1)); Delta(1, (P.of_int 2))]) in
  let m1_ = Mono (SemiRing.I,[Delta(1, (P.of_int 2))]) in
  let m2 = Mono (SemiRing.I,[Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))]) in
  let m2_ = Mono (SemiRing.I,[Delta(1, (P.of_int 2))]) in
  let m3 = Mono (SemiRing.I,[Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(3, (P.of_int 5))]) in 
  let m3_ = Mono (SemiRing.I,[Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4))]) in 
  let m4 = Mono (SemiRing.I,[Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(3, (P.of_int 4))]) in
  let m4_ = Mono (SemiRing.I,[Delta(1, (P.of_int 3))]) in
  (* let str1 = str_monomial (remove_index m4 (P.of_int 5)) in *)
  (* Printf.eprintf "%s = %s ?" str1 (str_monomial m4_); *)
  assert (mono_eq (remove_index m1 (P.of_int 1)) m1_);
  assert (mono_eq (remove_index m2 (P.of_int 3)) m2_);
  assert (mono_eq (remove_index m3 (P.of_int 5)) m3_);
  assert (mono_eq (remove_index m4 (P.of_int 4)) m4_)

let test_remove_tuple =
  let m1 = [Delta(1, (P.of_int 1)); Delta(1, (P.of_int 2))] in
  let m2 = [Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))] in
  let m3 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(3, (P.of_int 5))] in 
  let m4 = [Delta(1, (P.of_int 3)); Delta(2, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m5 = [Delta(1, (P.of_int 3)); Delta(3, (P.of_int 4)); Delta(4, (P.of_int 5))] in
  let m6 = [Delta(1, (P.of_int 3)); Delta(3, (P.of_int 4)); Delta(3, (P.of_int 5))] in

  let lm' = [m1;m2;m3;m4;m5;m6] in
  let lm = List.map (fun m -> Mono (SemiRing.I,m)) lm' in
  let m1' = List.nth lm 0 in
  let m2' = List.nth lm 1 in
  let m3' = List.nth lm 2 in
  let m4' = List.nth lm 3 in
  let m5' = List.nth lm 4 in
  let m6' = List.nth lm 5 in
  let ng = PTree.empty in
  let ng = List.fold_left (fun ng m -> insert_monomial ng m) ng lm in
    (* # m1 --2-- m2 *)
    (* # m3 --5--m4--4-- m5 *)
    (* #   \           / *)
    (* #    \         / *)
    (* #     4       5 *)
    (* #      \     / *)
    (* #       \   / *)
    (* #         m6 *)


  assert (
    match PTree.get (P.of_int 2) ng with
    | Some (dg,_,_) -> DeltaG.get (m1',m2') dg = None
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m3',m4') dg = Some (P.of_int 5)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m4',m3') dg = Some (P.of_int 5)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m4',m5') dg = Some (P.of_int 4)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m3',m6') dg = Some (P.of_int 4)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m5',m6') dg = Some (P.of_int 5)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m6',m5') dg = Some (P.of_int 5)
    | _ -> false);

  (* # Should remove m6 and m5 *)

  let dg' = (match PTree.get (P.of_int 3) ng with
  | Some dg -> remove_monomial m6' dg (P.of_int 5)
  | None -> assert false ) in
  let ng = PTree.set (P.of_int 3) dg' ng in

  assert (
    match PTree.get (P.of_int 2) ng with
    | Some (dg,_,_) -> DeltaG.get (m1',m2') dg = None
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m3',m4') dg = Some (P.of_int 5)
    | _ -> false);

  assert (
    match PTree.get (P.of_int 3) ng with
    | Some (dg,_,_) -> DeltaG.get (m4',m3') dg = Some (P.of_int 5)
    | _ -> false);

  match PTree.get (P.of_int 3) ng with
    | Some (dg,ml,pairs) -> 
        assert (not (List.mem m5' ml));
        assert (not (List.mem m6' ml));
        assert (not (List.mem (m6',m5') pairs));
        assert (not (List.mem (m5',m6') pairs));
        assert (DeltaG.get (m5',m6') dg = None);
        assert (DeltaG.get (m6',m5') dg = None);
    | _ -> assert false

let test_isfull =

  let m1 = [Delta(1, (P.of_int 2)); Delta(1, (P.of_int 3))] in
  let m2 = [Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))] in
  let m3 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(1, (P.of_int 4))] in 
  let m4 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(2, (P.of_int 4))] in
  let m5 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(3, (P.of_int 4))] in

  let lm' = [m1;m2;m3;m4;m5] in
  let lm = List.map (fun m -> Mono (SemiRing.I,m)) lm' in
  let m1' = List.nth lm 0 in
  let m2' = List.nth lm 1 in
  let m3' = List.nth lm 2 in
  let m4' = List.nth lm 3 in
  let m5' = List.nth lm 4 in
  let ng = PTree.empty in
  let ng = List.fold_left (fun ng m -> insert_monomial ng m) ng lm in

    (* # m1 --2-- m2 *)
    (* # m3 --3-- m4 *)
    (* #   \       | *)
    (* #    \      | *)
    (* #     3     3 *)
    (* #      \    | *)
    (* #       \   | *)
    (* #         m5 *)

  let dg2 = match PTree.get (P.of_int 2) ng with
            | Some dg -> dg
            | None -> assert false in
  let dg3 = match PTree.get (P.of_int 3) ng with
            | Some dg -> dg
            | None -> assert false in

  assert ((isfull dg3 m3' (P.of_int 4) 3) = true);
  assert ((isfull dg3 m4' (P.of_int 4) 3) = true);
  assert ((isfull dg3 m5' (P.of_int 4) 3) = true);
  assert ((isfull dg2 m1' (P.of_int 3) 3) = false);
  assert ((isfull dg2 m2' (P.of_int 3) 3) = false);
  assert ((isfull dg2 m1' (P.of_int 3) 2) = true);
  assert ((isfull dg2 m2' (P.of_int 3) 2) = true)

let test_fusion =
  let m1 = [Delta(1, (P.of_int 2)); Delta(1, (P.of_int 3))] in
  let m2 = [Delta(1, (P.of_int 2)); Delta(2, (P.of_int 3))] in
  let m3 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(1, (P.of_int 4))] in 
  let m4 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(2, (P.of_int 4))] in
  let m5 = [Delta(1, (P.of_int 2)); Delta(3, (P.of_int 3)); Delta(3, (P.of_int 4))] in

  let lm' = [m1;m2;m3;m4;m5] in
  let lm = List.map (fun m -> Mono (SemiRing.I,m)) lm' in
  let m0 = Mono (SemiRing.I, [Delta(1, (P.of_int 2))]) in
  let ng = PTree.empty in
  let ng = List.fold_left (fun ng m -> insert_monomial ng m) ng lm in

    (* # m1 --2-- m2 *)
    (* # m3 --3-- m4 *)
    (* #   \       | *)
    (* #    \      | *)
    (* #     3     3 *)
    (* #      \    | *)
    (* #       \   | *)
    (* #         m5 *)

  let ng,inf = fusion ng 3 in

  assert (PTree.get (P.of_int 3) ng = Some (DeltaG.empty,[],[]));
  assert (PTree.get (P.of_int 2) ng = Some (DeltaG.empty,[],[]));
  assert (match PTree.get (P.of_int 1) ng with
      | Some (dg,ml,pairs) -> mono_eq (List.hd ml) m0 && pairs = [] && dg = DeltaG.empty
      | None -> false
  )

let tests = 
  test_mono_diff;
  test_insert_tuple;
  test_remove_index;
  test_remove_tuple;
  Printf.eprintf "DeltaGraph tests OK!\n"(* }↑} *)

(* drawing stuff *)(* {↓{ *)

(* module DGraph = struct *)
(*   module V = struct *)
(*     type t = monomial *)
(*     let compare = (fun (n1,_) (n2,_) -> mono_compare n1 n2) *)
(*     let equal = (fun (n1,_) (n2,_) -> mono_eq n1 n2) *)
(*     let hash = Hashtbl.hash *)
(*   end *)
(*   module E = struct *)
(*     type t = V.t * V.t *)
(*     let compare = (fun (n11,n12) (n21,n22) -> (mono_compare n11 n21) + (mono_compare n21 n22)) *)
(*     let equal = (fun (n11,n12) (n21,n22) -> mono_eq n11 n21 && mono_eq n12 n22) *)
(*     let hash = Hashtbl.hash *)
(*     let src (b1,_) = b1 *)
(*     let dst (_,b2) = b2 *)
(*   end *)
(*   type t = deltaG *) 
(*   let iter_vertex (f:V.t->unit) (g:t) = *) 
(*     let (_,l,_) = g in *) 
(*     List.iter f l *)
(*   (1* let iter_succ f (g:t) ((_,tn):V.t) = *1) *) 
(*   (1*   let succs = (List.map (fun n -> n,get_term g n) (get_succ (Some tn))) in *1) *)
(*   (1*   List.iter f succs *1) *)
(*   let iter_edges_e (f:E.t->unit) (g:t) = *) 
(*     let (_,_,el) = g in *)
(*     List.iter f el *)
(* end *)

(* module Dot = *)
(*   Graph.Graphviz.Dot *)
(*     (struct *) 
(*       include DGraph *)
(*       let edge_attributes _ = [] *)
(*       let default_edge_attributes _ = [] *)
(*       let get_subgraph _ = None *)
(*       let vertex_name = str_monomial *)
(*       let vertex_attributes m = *)
(*         [`Shape `Box; `Label ( *)
(*                           String.concat "" [(str_monomial m)] *)
(*         )] *)
(*       let default_vertex_attributes _ = [] *)
(*       let graph_attributes _ = [] *)
(*     end) *)

(* let draw_deltagraph (dg:deltagraph) (name: string) : unit = *)
(*   let tmp,oc = Filename.open_temp_file (name^".") ".dot" in *)
(*   Dot.output_graph oc f.fn_graph ; *)
(*   let cmd = "dot -Tpdf "^tmp^" -o pdfs/"^name^".pdf" in *) 
(*   let _ = Sys.command cmd in *)
(*   close_out oc *)(* }↑} *)
*)
