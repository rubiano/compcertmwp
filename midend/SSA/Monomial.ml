open BinNums
open Registers
open Camlcoq
open MonomialBase

module type S =
  sig
    type t
    type scalar
    type choice (* orderedtype with compare*)
    type 'a delta
    type delta_choice = choice delta (* delta choice : choice * positive *)
    type n_deltagraphs

    val zero: t
    val one: t
    val smr_zero: scalar

    val prod: t -> t -> t
    val contains: t -> t -> bool
    val inclusion: t -> t -> inclusion

    val get_deltas: t -> delta_choice list
    val get_scalar: t -> scalar
    val get_product: t -> scalar * delta_choice list
    val get_index: 'a delta -> positive
    val isNull: t -> bool
    val cons: scalar * delta_choice list -> t
    val delta_cons: (choice*positive) -> delta_choice

    val compare: t -> t -> int
    val equal: t -> t -> bool
    val compare_deltas: delta_choice list -> delta_choice list -> check
    val insert_delta: delta_choice list -> delta_choice -> delta_choice list
    val insert_deltas: t -> delta_choice list -> t
    val makeDelta: choice*positive -> delta_choice
    val sum_scalar: t -> t -> scalar
    val correction: n_deltagraphs -> t -> reg*reg -> (n_deltagraphs*t)
    val to_str: t -> string
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val new_ndg: n_deltagraphs
  end

module Make(Choices: Set.S)(SemR: SemiRing.S):(S with type scalar = SemR.elt and type choice = Choices.elt) = struct

  module MonoBase =  Base(Choices)(SemR)
  module DeltaG = DeltaGraph.Make(MonoBase)
  type n_deltagraphs = DeltaG.n_deltagraphs

  type scalar = MonoBase.scalar

  type choice = MonoBase.choice
  type 'a delta = 'a MonoBase.delta
  type delta_choice = MonoBase.delta_choice
  type t = MonoBase.t

  let new_ndg = DeltaG.new_ndg
  let makeDelta (c,p:choice*positive) = MonoBase.Delta(c,p)
  let fusion = DeltaG.fusion

  let zero = MonoBase.zero
  let one = MonoBase.one
  let smr_zero = SemR.zero

  let get_deltas = MonoBase.get_deltas
  let get_scalar:t->SemR.elt = MonoBase.get_scalar
  let get_product:t->(SemR.elt*delta_choice list) = MonoBase.get_product
  let get_index:'a MonoBase.delta->positive = MonoBase.get_index
  let to_str = MonoBase.to_str

  let isNull (m:t) = SemR.isNull (get_scalar m)
  let cons ((sca,dl):(scalar*delta_choice list)) : t = MonoBase.cons (sca,dl)
  let sum_scalar (m1:t) (m2:t) = SemR.sum (get_scalar m1) (get_scalar m2)
  let delta_cons = MonoBase.delta_cons

  (* Compare two list of deltas 
   * Arguments:
     * deltas1, deltas2 : list of deltas to compare
   * Return: 
     * Equal if equality
     * Larger if deltas1 > deltas2
     * Smaller if deltas1 < deltas2
     *)
  let rec compare_deltas (deltas1:delta_choice list) (deltas2:delta_choice list) : check =
    match deltas1, deltas2 with
    | [], [] -> Equal
    | d::t, [] -> Larger
    | d1::t1, d2::t2 -> if d1 = d2 then compare_deltas t1 t2 else
      let MonoBase.Delta (i,j) = d1 in let MonoBase.Delta (m,n) = d2 in
      (*--------------------------↓ FIXME does it work with compare ? *)
      if (j<n) || (j == n && i < m) then Smaller else Larger
      (* we may prefer using compare *)
      (* if (j<n) || (j == n && (Choices.compare i m < 0)) then Smaller else Larger *)
    | ([], _) -> Smaller

  (* Compare two monomials 
   * If the scalars are equals, compare the deltas
   * Arguments:
     * m1, m2 : monomials to compare
   * Return: 
     * 0 if equal
     * 1 if m1 > m2
     * -1 if m1 < m2
     *)
  let compare (m1:t) (m2:t) : int =
    if get_scalar m1 = get_scalar m2 then
      match compare_deltas (get_deltas m1) (get_deltas m2) with
      | Equal -> 0
      | Larger -> 1
      | Smaller -> -1
    else SemR.compare (get_scalar m1) (get_scalar m2)

  let equal (m1:t) (m2:t) = (compare m1 m2 = 0)

  (* Takes as input a _sorted_ list of deltas and a delta. *)
  (* Check if two deltas have the same index: *)
  (* If they do, and if they: *)
  (* - disagree on the value expected, returns `[]` (empty list) *)
  (* - agree on the value expected, returns the original deltas *)
  (* If they don't: *)
  (*  add the new delta in the deltas "at the right position". *)
  (* Arguments: *)
  (*     sorted_deltas: list of deltas where to perform insert *)
  (*     delta: the delta value to be inserted *)
  (* Returns: *)
  (*     updated list of deltas. *)
  let rec insert_delta (sorted_deltas: delta_choice list) (delta: delta_choice) :
    (delta_choice) list =
    match sorted_deltas with
    | [] -> [delta]
    | (MonoBase.Delta (c1,i))::t -> let (MonoBase.Delta (c2,j)) = delta in 
        if P.(<) i j then
          let res = insert_delta t delta in
          if res = [] then res else (MonoBase.Delta (c1,i))::res
        else if i = j then
          if c1 = c2 then (MonoBase.Delta (c1,i))::t
          (* FIXME is it possible that deltas disagreed ? ↓ *)
          else []
        else delta::(MonoBase.Delta (c1,i))::t

  (* insert several deltas in a sorted list of deltas using insert_delta *)
  let insert_deltas (m:t) (deltas: delta_choice list) : t =
    let new_deltas = 
      List.fold_left (fun ds d -> insert_delta ds d) (get_deltas m) deltas in
    let new_scalar = if new_deltas = [] then SemR.zero else (get_scalar m) in
    MonoBase.cons (new_scalar,new_deltas)


  (* prod operation combines two monomials where *)
  (* The attributes of the resulting monomial are *)
  (* determined as follows: *)
  (* - output scalar is a product of the input scalars *)
  (* - two lists of deltas are merged according *)
  (*   to rules of insert_delta *)
  (* Arguments: *)
  (*     m1, m2: the monomials to combine *)
  (* Returns: *)
  (*     a new Monomial that is a product of two monomials *)
  let prod (m1:t) (m2:t) : t =
    let scalar = SemR.prod (get_scalar m1) (get_scalar m2) in
    (* Printf.printf "\tscalar = %s\n" (mwp_v_to_string scalar); *)
    if scalar = SemR.zero then zero else
    let m_prod = MonoBase.cons (scalar, get_deltas m1) in
    (* Printf.printf "\tinserting = %s\n" (str_monomial m_prod); *)
    if (get_deltas m2) <> [] then
      insert_deltas m_prod (get_deltas m2)
    else m_prod

  (*check if all deltas of m are in deltas of self *)
  (* Arguments: *)
  (*     self: this monomial *)
  (*     m: a monomial to search for intersection *)
  (* Returns: *)
  (*     False if one delta of m not in self *)
  (*     True otherwise *)
  let contains (self:t) (m:t) : bool =
    List.for_all (fun b -> List.mem b (get_deltas self)) (get_deltas m)


  (*gives info about inclusion of self monomial with monomial *)
  (* Arguments: *)
  (*     self: this monomial *)
  (*     monomial: a monomial to see inclusion *)
  (* Returns: *)
  (*     CONTAINS if self contains monomial *)
  (*     INCLUDED if self is included in monomial *)
  (*     EMPTY none of them *)
  let inclusion (self:t) (m:t) : inclusion =
    (* # self contains m ? *)
    let c:bool = contains self m in
    let summ = SemR.sum (get_scalar self) (get_scalar m) in
    (* # if self contains monomial and self.scalar >= m.scalar *)
    if c && (get_scalar m) = summ then
        CONTAINS
    else
        (*self included in m and self.scalar <= monomial.scalar *)
        let included:bool = contains m self in
        if included && (get_scalar self = summ) then
            INCLUDED
        else
            EMPTY

  let correction (ndg:n_deltagraphs) (mon:t) (i,j:reg*reg): (n_deltagraphs*t)=
    let scal,dl = get_product mon in
    if SemR.correction_cond scal (i,j) then
      let ndg = DeltaG.insert_monomial ndg mon in
      (ndg,MonoBase.cons (SemR.inf,dl))
    else (ndg,mon)
end

(* module OP = struct *)
(*   type t = positive *)
(*   (1* Order of enum type above ↑ *1) *)
(*   let compare = Stdlib.compare *) 
(*   let one = P.of_int 1 *)
(*   let of_int = P.of_int *)
(* end *)

(* module Choices = Set.Make(OP) *)

(* module Monomial_MWP = Make(Choices)(SR_MWP) *)

(* TOMOVE tests for the insert_delta function *)
(* let test_insert_delta = *)
(*     let deltas = [Monomial_MWP.makeDelta(P.of_int 1, P.of_int 1); Monomial_MWP.Delta(P.of_int 2, P.of_int 2); Monomial_MWP.Delta(P.of_int 2, P.of_int 4)] in *)
(*     let delta = Monomial_MWP.makeDelta(P.of_int 2, P.of_int 5) in *)
(*     let deltas = Monomial_MWP.insert_delta deltas delta in *)
(*     let expected = [Monomial_MWP.makeDelta(P.of_int 1, P.of_int 1); Monomial_MWP.Delta(P.of_int 2, P.of_int 2); Monomial_MWP.Delta(P.of_int 2, P.of_int 4); Monomial_MWP.Delta(P.of_int 2, P.of_int 5)] in *)
(*     assert (List.mem delta deltas); *)
(*     assert (deltas = expected); *)

(*     let deltas = [Monomial_MWP.makeDelta(P.of_int 1, P.of_int 1); Monomial_MWP.Delta(P.of_int 2, P.of_int 2); Monomial_MWP.Delta(P.of_int 2, P.of_int 4)] in *)
(*     let delta = Monomial_MWP.makeDelta(P.of_int 2, P.of_int 3) in *)
(*     let deltas = Monomial_MWP.insert_delta deltas delta in *)
(*     let expected = [Monomial_MWP.makeDelta(P.of_int 1, P.of_int 1); Monomial_MWP.Delta(P.of_int 2, P.of_int 2); Monomial_MWP.Delta(P.of_int 2, P.of_int 3); Monomial_MWP.Delta(P.of_int 2, P.of_int 4)] in *)
(*     assert (List.mem delta deltas); *)
(*     assert (deltas = expected) *)

(* (1* TOMOVE tests *1)(1* {↓{ *1) *)
(* let tests_monomial = *)
(*   test_insert_delta; *)
(*   Printf.eprintf "Monomial tests OK !\n"(1* }↑} *1) *)
