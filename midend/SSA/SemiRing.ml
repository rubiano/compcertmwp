open Registers

module type S =
  sig
    type elt
    val sum: elt -> elt -> elt
    val prod: elt -> elt -> elt
    val zero: elt
    val one: elt
    val inf: elt

    (* elt should be ordered type… *)
    val compare: elt -> elt -> int
    val isNull: elt -> bool
    val correction_cond: elt -> (reg*reg) -> bool
    val to_string: elt -> string
  end


(* The following build the product and sum of our semiring using *)
(* Maps of ordered mwp_value *)
module SR_MWP:S = struct
  type mwp_value = O | M | W | P | I

  let to_string (x:mwp_value) : string =
    match x with
    | O -> "O"
    | M -> "M"
    | W -> "W"
    | P -> "P"
    | I -> "I"

  module OrderedMWP = struct
    type t = mwp_value
    (* Order of enum type above ↑ *)
    let compare = Stdlib.compare 
  end

  type elt = OrderedMWP.t

  let compare = OrderedMWP.compare
  let correction_cond (scal:elt) (i,j:reg*reg): bool = 
    scal = P || (scal = W && i = j)

  let zero = O
  let one = M
  let inf = I

  let isNull = function | O -> true | _ -> false

  let mwp_values = [O;M;W;P;I]

  module MWPMap = Map.Make(OrderedMWP)

  let dicProd = 
    List.fold_left (fun (d2:(mwp_value MWPMap.t) MWPMap.t) smv1 -> 
      MWPMap.add smv1 
      ( List.fold_left (fun (d1:(mwp_value MWPMap.t)) smv2 -> 
          let res = (match smv1,smv2 with
          | O,_ -> O
          | _,O -> O
          | I,_ -> I
          | _,I -> I
          | P,_ -> P
          | _,P -> P
          | W,_ -> W
          | _,W -> W
          | M,M -> M) in
          MWPMap.add smv2 res d1) 
        MWPMap.empty mwp_values
      ) d2
    ) MWPMap.empty mwp_values

  let dicSum = 
    List.fold_left (fun d2 smv1 -> 
      MWPMap.add smv1 
      ( List.fold_left (fun d1 smv2 -> 
          let res = (match smv1,smv2 with
          | I,_ -> I
          | _,I -> I
          | P,_ -> P
          | _,P -> P
          | W,_ -> W
          | _,W -> W
          | M,_ -> M
          | _,M -> M
          | O,O -> O) in
          MWPMap.add smv2 res d1) 
        MWPMap.empty mwp_values
      ) d2
    ) MWPMap.empty mwp_values

  let sum (v1:mwp_value) (v2:mwp_value) : mwp_value =
    MWPMap.find v2 (MWPMap.find v1 dicSum)

  let prod (v1:mwp_value) (v2:mwp_value) : mwp_value =
    MWPMap.find v2 (MWPMap.find v1 dicProd)
end

