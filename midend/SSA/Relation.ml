open BinNums
open Maps
open Registers
open MWPUtils

module type S =
  sig
    type relation
    type matrix
    type polynomial
    type monomial
    type scalar
    type delta_choice
    type choice

    type possible_relations = relation list
    type n_deltagraphs
    val empty_possible_relations: possible_relations
    val identity_possible_relations: possible_relations
    val poly_zero: polynomial
    val poly_unit: polynomial
    val new_ndg: n_deltagraphs

    val identity_relation: reg list -> relation
    val replace_column: relation -> (reg*polynomial) list -> reg -> relation
    val is_empty: relation -> bool
    val matrix_resize: relation -> reg list -> relation
    val homogeneisation: relation -> relation -> relation*relation
    val sum: relation -> relation -> relation
    val prod: relation -> relation -> relation
    val p_rel_sum: possible_relations -> possible_relations -> possible_relations
    val not_contains_matrix: relation list -> matrix -> bool
    val contains_matrix: relation list -> matrix -> bool
    val composition: possible_relations -> possible_relations -> possible_relations
    val equal: relation -> relation -> bool
    val fixpoint: relation -> relation
    val p_relations_fixpoint: possible_relations -> possible_relations
    val loop_correction: n_deltagraphs -> relation -> (n_deltagraphs*relation)
    val p_relations_loop_correction: n_deltagraphs ref-> possible_relations -> (n_deltagraphs*possible_relations)
    val sum_all: possible_relations list -> possible_relations
    val print_relation: relation -> unit
    val print_possible_relation: possible_relations -> unit
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val poly_cons: monomial list -> polynomial
    val mono_cons: scalar * delta_choice list -> monomial
    val delta_cons: (choice*positive) -> delta_choice
  end

module Make(Choices: Set.S)(SemR: SemiRing.S):(S with type scalar = SemR.elt and type choice = Choices.elt) = struct
  module Matrix = Matrix.Make(Choices)(SemR)
  type matrix = Matrix.t
  type n_deltagraphs = Matrix.n_deltagraphs
  type polynomial = Matrix.polynomial
  type monomial = Matrix.monomial

  let empty_possible_relations = []
  let fusion = Matrix.fusion
  let new_ndg = Matrix.new_ndg

  type scalar = SemR.elt
  type choice = Choices.elt

  type delta_choice = Matrix.delta_choice
  let poly_cons = Matrix.poly_cons
  let mono_cons = Matrix.mono_cons
  let delta_cons = Matrix.delta_cons
  let poly_zero = Matrix.poly_zero
  let poly_unit = Matrix.poly_unit

  (* a relation is a list of vars (here registers) *)
  (* and a matrix of polynomial *)
  type relation = {
    vars : reg list;
    matrix : matrix
  }

  let print_relation (r:relation) : unit =
    Printf.eprintf "\t|\t";
    List.iter (fun v -> 
      Printf.eprintf "%s\t|\t " (str_reg (v));
    ) r.vars;
    Printf.eprintf "\n";
    List.iter (fun i -> 
      Printf.eprintf "%s\t|\t " (str_reg (i));
      List.iter (fun j -> 
        let p = Matrix.get_polynome_at r.matrix i j in
        Printf.eprintf "%s\t|\t" (Matrix.str_polynomial p)
      ) r.vars;
      Printf.eprintf "\n"
    ) r.vars

  (* same as RelationList *)
  type possible_relations = relation list

  let print_possible_relation (p_r:possible_relations) : unit =
    Printf.eprintf "*** Possible Relations *** \n";
    List.iteri (fun i r ->
      Printf.eprintf "Relation %d :\n" i;
      print_relation r
    ) p_r;
    Printf.eprintf "\n";
    Printf.eprintf "************************** \n"

  (* build the identity_relation *) 
  let identity_relation (vars:reg list) : relation =
    let matrix = Matrix.identity_matrix vars in 
    {vars; matrix}

  let identity_possible_relations = [identity_relation []]

    (* Insert the polynomial for each registers at the right place in the matrix *)
  (* Arguments: *)
    (*   rel: the relation (identity with the right registers) in which we add the polys *)
    (*   polys: the polys to add *)
    (*   dst: the register where to insert this column *)
  (* Returns: *)
    (*   new relation after applying the column insertion *)
  let replace_column (rel:relation) (polys:(reg * polynomial) list) (dst:reg) : relation =
    let t_polys = List.fold_left (fun acc (v,p) -> PTree.set v p acc) PTree.empty polys in
    let matrix = PTree.set dst t_polys rel.matrix in
    let vars = rel.vars in {vars; matrix}

  let is_empty (r:relation) : bool =
    List.length r.vars = 0

  (* resize the matrix of `r` regarding to the `vars` given *)
  let matrix_resize (r: relation) (vars: reg list) : relation =
    let matrix = List.fold_left (fun mat' reg ->
      if List.mem reg r.vars then mat' else
      let line = List.fold_left (fun line vj ->
        (* FIXME fill others with poly_zero or just don't set it at all ? *)
        if reg = vj then PTree.set vj Matrix.poly_unit line else line
        (* if reg = vj then PTree.set vj poly_unit line else PTree.set vj poly_zero line *)
      ) PTree.empty vars in
      PTree.set reg line mat'
    ) r.matrix vars in
    {vars;matrix}

  (* Performs homogenisation on two relations. *)
  (* After this operation both relations will have same *)
  (* variables and their matrices of the same size. *)
  (* This operation will internally resize matrices as needed. *)
  (* Arguments: *)
  (*     r1: first relation to homogenise *)
  (*     r2: second relation to homogenise *)
  (* Returns: *)
  (*     Homogenised versions of the 2 inputs relations *)
  let homogeneisation (r1: relation) (r2: relation): relation * relation =
    (*     # check empty cases *)
    if is_empty r1 then (identity_relation r2.vars), r2
    else
      if is_empty r2 then r1, (identity_relation r1.vars)
      else
      (* check equality *)
      if List.for_all2 (fun v1 v2 -> Reg.eq v1 v2) r1.vars r2.vars then
        r1,r2
      else
          let extended_vars =
            (* could use List.sort_uniq here ? *)
            (*     # build a list of all distinct variables; maintain order *)
            r1.vars @ (List.filter (fun v -> not (List.mem v r1.vars)) r2.vars) in
          (* resize matrix by adding missing registers *)
          let r1 = matrix_resize r1 extended_vars in
          let r2 = matrix_resize r2 extended_vars in
          r1,r2
          (* I may have forgotten stuff about indexes here… FIXME *)

  (* Sum two relations. *)
  (* Arguments: *)
  (*     r1: Relation to sum with r2. *)
  (* Returns: *)
  (*    A new relation that is a sum of inputs. *)
  let relation_sum (r1:relation) (r2:relation) : relation =
    let (r1, r2) = homogeneisation r1 r2 in
    let matrix = Matrix.sum r1.vars r1.matrix r2.matrix in
    let vars = r1.vars in
    {vars;matrix}
  let sum = relation_sum

  (* Sum of possible_relations (relation list) *)
  (* Arguments: *)
  (*     sum: possible relations to sum with p_r. *)
  (* Returns: *)
  (*    A new relation list that is a sum of inputs. *)
  let p_rel_sum (sum:possible_relations) (p_r:possible_relations) : possible_relations =
    (* for each possible relation in the current accumulater `sum`, sum all possible relations of `p_r` *)
    List.map (fun r -> List.fold_left (relation_sum) r p_r) sum

  (* sum all the possible relations in the input *)
  let sum_all (p_relations_to_sum:possible_relations list) : possible_relations =
    match p_relations_to_sum with
    | [] -> []
    | [r] -> r
    | h::t -> List.fold_left (p_rel_sum) h t

  (* the product or composition of relation r1 * r2 *)
  let relation_prod (r1:relation) (r2:relation) : relation =
    let r1, r2 = homogeneisation r1 r2 in
    (* Printf.eprintf "PROD R1:\n"; *)
    (* print_relation r1; *)
    (* Printf.eprintf "PROD R2:\n"; *)
    (* print_relation r2; *)
    let matrix = Matrix.prod r1.vars r1.matrix r2.matrix in
    let vars = r1.vars in
    (* Printf.eprintf "PROD RES:\n"; *)
    (* print_relation {vars;matrix}; *)
    {vars;matrix}
  let prod = relation_prod

  let not_contains_matrix (rl:relation list) (m:matrix) : bool =
    List.for_all (fun r -> r.matrix <> m) rl

  let contains_matrix (rl:relation list) (m:matrix) : bool =
    List.exists (fun r -> r.matrix = m) rl

  (* the composition of possibles relation p_r1 * p_r2 *)
  let composition (p_r1:possible_relations) (p_r2:possible_relations) : possible_relations =
    if p_r2 = [] then p_r1
      else if p_r1 = [] then p_r2
    else
    List.fold_left (fun acc r1 -> 
      List.fold_left (fun acc' r2 -> 
        let outr = (relation_prod r1 r2) in
        if not (contains_matrix acc' outr.matrix) then outr::acc'
        else acc'
      ) acc p_r2
    ) [] p_r1

  (* relation equality *)
  let relation_eq (r1:relation) (r2:relation) : bool =
    (* Printf.eprintf "↓ Relation equality ? ↓\n"; *)
    (* PrintMWP.print_relation r1; *)
    (* PrintMWP.print_relation r2; *)
    (* Printf.eprintf "↑ Relation equality ? ↑\n"; *)
    if List.for_all2 (fun v1 v2 -> v1 = v2) r1.vars r2.vars then
      List.for_all (fun i -> 
        List.for_all (fun j -> 
          Matrix.poly_eq (Matrix.get_polynome_at r1.matrix i j) (Matrix.get_polynome_at r2.matrix i j)
        ) r1.vars
      ) r1.vars
    else false
  let equal = relation_eq

  (* put M on diagonal where O *)
  let sum_with_identity (r:relation) : relation =
    let matrix = List.fold_left (fun mat v ->
      let p = Matrix.get_polynome_at mat v v in
      if p = Matrix.poly_zero then
        Matrix.set_polynome_at mat v v Matrix.poly_unit
      else mat
    ) r.matrix r.vars in
    let vars = r.vars in
    {vars; matrix}

  let rec relation_fixpoint' (r:relation) (current:relation) (fix:relation) (max:int): relation =
    (* Printf.printf "in relation_fixpoint:\n"; *)
    let new_current = relation_prod fix r in
    (* let new_current = relation_prod current r in *)
    (* Printf.eprintf "fixpoint new_current:\n"; *)
    (* print_relation new_current; *)
    let new_fix = sum_with_identity new_current in
    (* Printf.eprintf "fixpoint new_fix:\n"; *)
    (* print_relation new_fix; *)
    if max = 0 then 
      new_fix
    else 
      if relation_eq new_fix fix then
        (Printf.eprintf "equal at %d\n" (20-max);
        new_fix)
      else relation_fixpoint' r new_current new_fix (max-1)

  (* compositional fixpoint of a relation r *)
  let relation_fixpoint (r:relation) : relation =
    let idr = identity_relation r.vars in
    relation_fixpoint' r idr idr 20
  let fixpoint = relation_fixpoint

  let p_relations_fixpoint (pr:possible_relations) : possible_relations =
    List.map relation_fixpoint pr

  let relation_loop_correction (ndg:n_deltagraphs) (r:relation) : (n_deltagraphs*relation) =
    let (ndg,matrix) =  Matrix.loop_correction ndg r.matrix r.vars in
    let vars = r.vars in
    (ndg,{vars;matrix})
  let loop_correction = relation_loop_correction

  let p_relations_loop_correction (ndg:n_deltagraphs ref) (pr:possible_relations) : (n_deltagraphs*possible_relations) = 
    let rndg,pr = List.fold_left 
      (fun (ndg,pr') r -> 
        let (ndg',r') = relation_loop_correction ndg r in
        (* FIXME r'::pr' or pr'::r' ? *)
        (ndg',r'::pr')
        ) (!ndg,[]) pr in
    (* FIXME Remove all those ref ! *)
    ndg := rndg;
    (rndg,pr)
    (* List.map (relation_loop_correction ndg) pr *)
end
