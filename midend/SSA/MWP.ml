(* open BinNums *)
open Maps
open AST
open SSA
open Op
(* open Camlcoq *)
open Registers

module SR_MWP = struct

  type elt = O | M | W | P | I

  module OrderedMWP = struct
    type t = elt
    (* Order of enum type above ↑ *)
    let compare = Stdlib.compare 
  end

  let to_string (x:elt) : string =
    match x with
    | O -> "O"
    | M -> "M"
    | W -> "W"
    | P -> "P"
    | I -> "I"

  let w = W
  let p = P

  let compare = OrderedMWP.compare
  let correction_cond (scal:elt) (i,j:reg*reg): bool = 
    scal = P || (scal = W && i = j)

  let zero = O
  let one = M
  let inf = I

  let isNull = function | O -> true | _ -> false

  let elts = [O;M;W;P;I]

  module MWPMap = Map.Make(OrderedMWP)

  let dicProd = 
    List.fold_left (fun (d2:(elt MWPMap.t) MWPMap.t) smv1 -> 
      MWPMap.add smv1 
      ( List.fold_left (fun (d1:(elt MWPMap.t)) smv2 -> 
          let res = (match smv1,smv2 with
          | O,_ -> O
          | _,O -> O
          | I,_ -> I
          | _,I -> I
          | P,_ -> P
          | _,P -> P
          | W,_ -> W
          | _,W -> W
          | M,M -> M) in
          MWPMap.add smv2 res d1) 
        MWPMap.empty elts
      ) d2
    ) MWPMap.empty elts

  let dicSum = 
    List.fold_left (fun d2 smv1 -> 
      MWPMap.add smv1 
      ( List.fold_left (fun d1 smv2 -> 
          let res = (match smv1,smv2 with
          | I,_ -> I
          | _,I -> I
          | P,_ -> P
          | _,P -> P
          | W,_ -> W
          | _,W -> W
          | M,_ -> M
          | _,M -> M
          | O,O -> O) in
          MWPMap.add smv2 res d1) 
        MWPMap.empty elts
      ) d2
    ) MWPMap.empty elts

  let sum (v1:elt) (v2:elt) : elt =
    MWPMap.find v2 (MWPMap.find v1 dicSum)

  let prod (v1:elt) (v2:elt) : elt =
    MWPMap.find v2 (MWPMap.find v1 dicProd)
end


(* The following build the product and sum of our semiring using *)
(* Maps of ordered mwp_value *)
module MWP_RA:(DFAnalysis.RA) = struct

  module OP = struct
    type t = int
    (* Order of enum type above ↑ *)
    let compare = Stdlib.compare 
  end

  module Choices = Set.Make(OP)

  module Rel = Relation.Make(Choices)(SR_MWP)
  
  type polynomial = Rel.polynomial
  type relation = Rel.relation
  type possible_relations = Rel.possible_relations
  type n_deltagraphs = Rel.n_deltagraphs

  let empty_possible_relations = Rel.empty_possible_relations
  let composition = Rel.composition
  let identity_possible_relations = Rel.identity_possible_relations
  let p_relations_fixpoint = Rel.p_relations_fixpoint
  let p_relations_loop_correction = Rel.p_relations_loop_correction
  let sum_all = Rel.sum_all
  let fusion = Rel.fusion
  let new_ndg = Rel.new_ndg
  let print_possible_relation = Rel.print_possible_relation

  (* Create a polynomial vector. *)
  (* For an SSA node that represents a binary operation, this method *)
  (* generates a vector of polynomials based on the properties of that *)
  (* node. *)
  (* The returned value depends on the type of operator, how many *)
  (* operands are constants, and if the operands are equal. *)

  (* Arguments: *)
  (*     args: the Arguments of the operation *)
  (*     dst: the node is used as delta index *)
  (*     op: the operation *)
  (* Returns: *)
  (*     list of Polynomial vectors *)
  let create_vector (args:reg list) (dst:reg) (op:Op.operation) : (reg * polynomial) list =
    (* I'm only showing simple main cases… *)
    match op, args with
    (* multiplication *) 
    | Omul, [r1; r2] -> 
        let (poly:polynomial) = 
          Rel.poly_cons [
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (0, dst)]);
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (1, dst)]);
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (2, dst)])
          ] in
        let (poly2:polynomial) = 
          Rel.poly_cons [
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (0, dst)]);
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (1, dst)]);
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (2, dst)])
          ] in
        [(r1,poly);(r2,poly2)]
    (* binary Add operation in compcert will often
     * look like : dst = r1 + r2 + 0 (int) *)
    | Olea (Aindexed2 BinNums.Z0), [r1; r2] -> 
        let (poly:polynomial) = 
          Rel.poly_cons [
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (0, dst)]);
            Rel.mono_cons (SR_MWP.one, [Rel.delta_cons (1, dst)]);
            Rel.mono_cons (SR_MWP.p, [Rel.delta_cons (2, dst)])
          ] in
        let (poly2:polynomial) = 
          Rel.poly_cons [
            Rel.mono_cons (SR_MWP.w, [Rel.delta_cons (0, dst)]);
            Rel.mono_cons (SR_MWP.p, [Rel.delta_cons (1, dst)]);
            Rel.mono_cons (SR_MWP.one, [Rel.delta_cons (2, dst)])
          ] in
        [r1,poly;r2,poly2]
        (* poly_zero otherwise *)
    | _ -> [dst,Rel.poly_zero]
        (* to be continued TODO *)

  (* Compute relation for an SSA instruction. *)
  (* Arguments: *)
  (*     i: The SSA instruction *)
  (* Returns: *)
  (*      the possibles relation for that instruction *)
  let rel_instruction (i:instruction) : possible_relations =
    match i with
    | Inop s -> []
    | Iop (op, args, dst, s) -> 
        let listvars = dst::args in
        let rel = Rel.identity_relation listvars in
        let polys = create_vector args dst op in
        [Rel.replace_column rel polys dst]
    (* | Icond (c,regs,n1,n2) -> (1* branching *1) *)
    (* | Ijumptable of reg * node list (1* multi branching *1) *)
    | _ -> [] (* TODO *)

    (* Iphi ([b,c],a) means a = b or a = c 
     *   a  b  c
     * a O  M  M
     * b O  M  O
     * c O  O  M
     *)
  let rel_phi (phi:SSA.phiinstruction) : possible_relations =
    match phi with
    (* actually phi have as many args as predecessors… here it's only for two *)
    (* TODO rewrite it for as many args… *)
    | Iphi ([r1;r2],dst) -> 
        (* assert (List.length args = 2); *)
        let listvars = [dst;r1;r2] in
        let rel = Rel.identity_relation listvars in
        let polys = [dst,Rel.poly_zero;r1,Rel.poly_unit;r2,Rel.poly_unit] in
        [Rel.replace_column rel polys dst]
    | _ -> assert false

end

module MWP_Analysis = DFAnalysis.Make(MWP_RA)

(* let test_rel_phi = *)
(*   let a = P.of_int 1 in *)
(*   let b = P.of_int 2 in *)
(*   let c = P.of_int 3 in *)
(*   let p_r = rel_phi (Iphi([b;c],a)) in *)
(*   let r = List.hd p_r in *)
(*   let aa = get_polynome_at r.matrix a a in *)
(*   assert (aa = poly_zero); *)
(*   let ab = get_polynome_at r.matrix a b in *)
(*   assert (ab = poly_unit); *)
(*   let ac = get_polynome_at r.matrix a c in *)
(*   assert (ac = poly_unit); *)
(*   let bb = get_polynome_at r.matrix b b in *)
(*   assert (bb = poly_unit); *)
(*   let cc = get_polynome_at r.matrix c c in *)
(*   assert (cc = poly_unit); *)
(*   Printf.eprintf "relphi OK !\n" *)

let mwp_function (id:BinNums.positive) (f:SSA.coq_function) : unit =
  (* I use ref here to not deal with index on all function signatures *)
  (* But in Coq we will have to manage that… *)
  let rel_map = ref PTree.empty in
  (* predecessors will help us finding junction point *)
  (* let preds = make_predecessors f.SSA.fn_code SSA.successors_instr in *)
  (* Printf.printf "Entry : %ld" (P.to_int32 f.SSA.fn_entrypoint); *)

  (* n delta_graph with a reference *)
  let ndg = ref MWP_Analysis.new_ndg in

  let p_r = MWP_Analysis.compute_relation ndg f.SSA.fn_code f.SSA.fn_phicode [] rel_map f.SSA.fn_entrypoint in
  MWP_Analysis.print_possible_relation p_r

let analyse_globdef (id, gd) =
  match gd with
  | Gfun(Internal f) -> 
      mwp_function id f
  | _ -> ()

let analyse_program (prog: SSA.program) : unit =
  List.iter (analyse_globdef) prog.prog_defs

let mwp_test : unit =
  ()
  (* DeltaGraph.tests; *)
  (* Matrix.test_matrix; *)
  (* Monomial.tests_monomial; *)
  (* Polynomial.poly_tests; *)
  (* test_rel_phi *)
