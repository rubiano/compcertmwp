open Maps
open Registers
open SSA
(* open Camlcoq *)

module type S =
  sig
    type polynomial
    type relation
    type possible_relations
    type n_deltagraphs
    val compute_relation: n_deltagraphs ref -> instruction PTree.t -> phicode -> node list -> possible_relations PTree.t ref -> node -> possible_relations
    val new_ndg: n_deltagraphs
    val print_possible_relation: possible_relations -> unit
  end

module type RA =
  sig
    type possible_relations
    type polynomial
    type n_deltagraphs
    type relation

    val empty_possible_relations: possible_relations
    val composition: possible_relations -> possible_relations -> possible_relations
    val identity_possible_relations: possible_relations
    val p_relations_fixpoint: possible_relations -> possible_relations
    val sum_all: possible_relations list -> possible_relations
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val p_relations_loop_correction: n_deltagraphs ref -> possible_relations -> (n_deltagraphs*possible_relations)
    val new_ndg: n_deltagraphs
    val print_possible_relation: possible_relations -> unit

    val rel_phi: phiinstruction -> possible_relations
    val rel_instruction: instruction -> possible_relations
    val create_vector: reg list -> reg -> Op.operation -> (reg*polynomial) list
  end

module Make(RelA: RA):(S with type possible_relations = RelA.possible_relations and type relation = RelA.relation and type polynomial = RelA.polynomial and type n_deltagraphs = RelA.n_deltagraphs) = struct

  type polynomial = RelA.polynomial
  type relation = RelA.relation
  type possible_relations = RelA.possible_relations
  type n_deltagraphs = RelA.n_deltagraphs

  let print_possible_relation = RelA.print_possible_relation
  let new_ndg = RelA.new_ndg
  let get_inst_at (code:SSA.instruction PTree.t) (node:SSA.node) : SSA.instruction =
      match (PTree.get node code) with
      | None -> failwith ("get_inst_at: unknown node")
      | Some i -> i

  let get_phis_at (code:SSA.phicode) (node:SSA.node) : SSA.phiblock =
      match (PTree.get node code) with
      | None -> raise Not_found
      | Some i -> i

  let get_rel_at (rel_map:possible_relations PTree.t) (node:SSA.node) : possible_relations =
      match (PTree.get node rel_map) with
      | None -> failwith ("get_rel_at: unknown node")
      | Some i -> i

  (* Return the list of successors nodes for a given SSA instruction *)
  let successors_instr = function
    | Inop s -> [s]
    | Iop (_, _, _, s) -> [s]
    | Iload (_, _, _, _, s) -> [s]
    | Istore (_, _, _, _, s) -> [s]
    | Icall (_, _, _, _, s) -> [s]
    | Ibuiltin (_, _, _, s) -> [s]
    | Icond (_, _, s1, s2) -> [s1;s2]
    | Ijumptable (_, tbl) -> tbl
    | Ireturn (_) -> []
    | _ -> assert false

    (* get the nodes in the loop regarding to the path *)
  let rec get_loop_nodes_from (head:SSA.node) (path:SSA.node list) : (SSA.node list) =
    match path with
    | [] -> failwith ("head not found")
    | h::t -> if h = head then h::t else get_loop_nodes_from head t

    (* computes the relation for the program point `node` recursively computing the relations for the successor of that node *)
  let rec compute_relation (ndg:n_deltagraphs ref) (code:SSA.instruction PTree.t) (phicode:SSA.phicode) (path:SSA.node list) (rel_map:possible_relations PTree.t ref) (node:SSA.node) : possible_relations =

    (* simpl_print_inst (get_inst_at node code); *)
    if List.mem node path then

      (* Already visited node → loop detection *)
      let loop_nodes = get_loop_nodes_from node path in
      (* Printf.printf "Loop with node  %ld :\n" (P.to_int32 node); *)

      (* get relations of each statement in the loop *)
      (* Printf.eprintf "Path :\n"; *)
      (* List.iter (fun n -> Printf.eprintf "%ld," (P.to_int32 n)) path; *)
      (* Printf.printf "\nNodes In loop :\n"; *)
      (* List.iter (fun n -> Printf.printf "%ld," (P.to_int32 n)) loop_nodes; *)

      (* get the relations for each node in the loop *) 
      match List.map (get_rel_at !rel_map) loop_nodes with
      | [] -> Printf.eprintf "empty loop ?"; RelA.empty_possible_relations
      | h::t_relations ->
        (* Printf.eprintf "\nBEGIN LOOP :\n"; *)
        (* List.iter print_possible_relation (h::t_relations); *)
        (* Printf.eprintf "\nEND LOOP *** \n"; *)

        (* compute the composition of the entire loop *) 
        let loop_relation = List.fold_left RelA.composition h t_relations in

        (* Printf.eprintf "\nLoop FIXP of :\n"; *)
        (* print_possible_relation loop_relation; *)
        (* Printf.eprintf "\nEND Loop FIXP *** \n"; *)

        (* compute the fixpoint *) 
        let loop_fixp = RelA.p_relations_fixpoint loop_relation in
        (* Printf.eprintf "\nLoop correction… of :\n"; *)
        (* print_possible_relation loop_fixp; *)
        (* Printf.eprintf "\nEND Loop correction *** \n"; *)

        (* compute the correction and enrich the n_deltagraph *)
        let (_,loop_correction) = RelA.p_relations_loop_correction ndg loop_fixp in
        (* Printf.printf "\nLoop corrected : \n"; *)
        (* print_possible_relation loop_correction; *)
        
        rel_map := PTree.set node (loop_correction) !rel_map;

        (* PrintMWP.printDeltaGraph ndg; *)

        (* simplify the n_deltagraph (infinite = true if no evaluation) *) 
        let rndg,infinite = (RelA.fusion ndg 3) in
        ndg := rndg;

        (* PrintMWP.printDeltaGraph ndg; *)

        if infinite then (
          Printf.printf "\nDeltaGraph infinite !\n";
          exit 0)
        else Printf.printf "\n";

        loop_correction
    else
      (* Not visited node yet *)
      let inst = get_inst_at code node in
      
      (* compute de relations of phi instructions *) 
      (* NOTE a node can have several phi instructions *)
      let rel_phis = try (
        (* get phi instructions at node `node` *)
        let phis = get_phis_at phicode node in
        (* compute the relation of each phi *)
        let rels_phis = List.map (RelA.rel_phi) phis in
        (* Compose the phis *)
        List.fold_left (fun acc rphi -> RelA.composition acc rphi) RelA.identity_possible_relations rels_phis
      ) with Not_found -> RelA.identity_possible_relations in

      (* Printf.printf "\n___"; *)
      (* PrintSSA.print_phi phicode node stdout; *)
      (* PrintSSA.print_instruction phicode stdout (P.to_int node,node,inst); *)
      (* Printf.printf "___"; *)

      (* Printf.eprintf "rel_phis:\n"; *)
      (* print_possible_relation rel_phis; *)

      (* NOTE if it has phi instruction, the instruction should be a nop *)
      let rel_inst = RelA.rel_instruction inst in

      (* print_possible_relation rel_inst; *)
      (* compose phis and the instruction *)
      let rel_inst = RelA.composition rel_phis rel_inst in

      (* Printf.eprintf "After composition with phi :"; *)
      (* print_possible_relation rel_inst; *)

      (* get successors *) 
      let succs = successors_instr inst in
      (* record the relation of this node *)
      rel_map := PTree.set node (rel_inst) !rel_map;
      (* update path *)
      let path = path@[node] in
      (* define the recursive function *) 
      let rec_call = compute_relation ndg code phicode path rel_map in

      (* Printf.printf "Node %d: Succs: %d" (P.to_int node) (List.length succs); *)

      (* XXX recursive call to successors XXX *)
      let sub_rels = List.map (rec_call) succs in

      (* Printf.eprintf "\nNode %d: Sub_rels %d :\n" (P.to_int node) (List.length sub_rels); *) 
      (* List.iter (print_possible_relation) sub_rels; *)
      (* Printf.eprintf "\n"; *)

      (* if several successors, sum them all *)
      let sum_sub = (RelA.sum_all sub_rels) in

      (* Printf.eprintf "\nNode %d: Sum_subs:\n" (P.to_int node) ; *)
      (* print_possible_relation sum_sub; *)
      (* Printf.eprintf "\n___\n"; *)
      (* Printf.printf "\nNode %d: Composition with___:\n" (P.to_int node) ; *)
      (* print_possible_relation rel_inst; *)
      (* Printf.printf "\n___\n"; *)

      (* compose the sum for successors with this relation *)
      let pr_comp = RelA.composition rel_inst sum_sub in

      (* Printf.eprintf "\nNode %d: Result:\n" (P.to_int node) ; *)
      (* print_possible_relation pr_comp; *)
      (* Printf.eprintf "\n___\n"; *)
      pr_comp
end
