open Registers
open MWPUtils
open BinNums

module type S = 
  sig
    type t
    type monomial
    type n_deltagraphs
    type scalar
    type delta_choice
    type choice
    val zero: t
    val one: t
    val sum: t -> t -> t
    val prod: t -> t -> t
    val compare: t -> t -> int
    val equal: t -> t -> bool
    val correction: n_deltagraphs -> t -> (reg*reg) -> (n_deltagraphs*t)
    val to_str: t -> string
    val cons: monomial list -> t
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val mono_cons: scalar * delta_choice list -> monomial
    val delta_cons: (choice*positive) -> delta_choice
    val new_ndg: n_deltagraphs
  end

module Make(Choices: Set.S)(SemR: SemiRing.S):(S with type scalar = SemR.elt and type choice = Choices.elt) = struct
  module Mono = Monomial.Make(Choices)(SemR)
(* type polynomial = monomial list *)
  type t = Mono.t list
  type monomial = Mono.t
  let zero = [Mono.zero]
  let one = [Mono.one]
  let poly_zero = zero
  type n_deltagraphs = Mono.n_deltagraphs

  type scalar = SemR.elt
  type delta_choice = Mono.delta_choice
  type choice = Choices.elt

  let cons (ml:monomial list): t = ml
  let mono_cons = Mono.cons
  let delta_cons = Mono.delta_cons
  let fusion = Mono.fusion
  let new_ndg = Mono.new_ndg

  let to_str (p:t) : string =
    match p with
    | [] -> Mono.to_str Mono.zero
    | monos -> List.fold_left (fun s m -> Printf.sprintf "%s+%s" s (Mono.to_str m)) "" monos

  (* change scalar at a given rank in a polynomial *)
  let rec change_scalar_at (s:Mono.scalar) (n:int) (p:t) : t =
    match p with
    | [] -> []
    | m::t -> 
      let s' = Mono.get_scalar m in
      let dl = Mono.get_deltas m in
      if n = 0
      then (Mono.cons(s,dl))::t
      else (Mono.cons(s',dl))::(change_scalar_at s (n-1) t)

  (* sort the given polynomial (monomial list) *)
  let poly_sort (p:t) : t =
    (* base case *)
    if p = [] then [] else
    let len = List.length p in
    if len < 2 then
      if Mono.isNull (List.hd p) then [] else p
    else
      (* does not remove any equal but with not same scalar FIXME *)
      let p = List.sort Mono.compare p in
      (* remove all monomial with zero as scalar *)
      List.filter (fun m -> not (Mono.isNull m)) p

  (*filter list_monom regarding to mono inclusion and return info *)
  (*remove all monomials of list_monom that are included in mono. *)
  (*return CONTAINS if one of monomials of list_monom contains mono *)
  (*(regarding to Monomial.inclusion def). *)
  (*Arguments: *)
  (*    list_monom: a list of monomials *)
  (*    mono: a monomial we want to add *)
  (*    i: the position index where to add mono *)
  (*Returns: *)
  (*    False if mono already in list_monom and shifted index where to *)
  (*    insert mono, return True if mono not in list_monom *)
  let rec poly_inclusion' (res:t) (list_monom: monomial list) (mono: monomial) (i:int) (j:int) : (bool * monomial list * int) =
    match list_monom with
    | [] -> true, res, i (* No inclusion *)
    | m::t -> let incl = Mono.inclusion m mono in
        (* # if m ⊆ mono *)
        if incl = MonomialBase.CONTAINS then
          (* We will then add mono so we can remove m *)
          (* Printf.eprintf "\t\t %s CONTAINS %s \n" (str_polynomial list_monom) (str_monomial mono); *)
          let new_res = List.filter (fun x -> x <> m) res in
          (*If removed monom is before i (where we want to insert mono) *)
          let i = if j < i then
            i - 1  (* shift left position *)
          else i in poly_inclusion' new_res new_res mono i (j+1)
        else if incl = MonomialBase.INCLUDED then
          (* We don't want to add mono, inform with CONTAINS *)
          (* Printf.eprintf "\t\t %s INCLUDED IN %s \n"  (str_monomial mono) (str_polynomial list_monom); *)
          (false, res, i)
        else poly_inclusion' res t mono i (j+1)

  let poly_inclusion (list_monom: monomial list) (mono: monomial) (i:int) (j:int) : (bool * monomial list * int) =
    poly_inclusion' list_monom list_monom mono i j

  (* the zip_sum used in sum *)
  let rec zip_sum (p1:t) (p2:t) (i:int) =
    match p2 with
    | [] -> p1
    | mono2::p2' -> 
          (* see definition of poly_inclusion *)
          let to_be_inserted, new_poly, i = poly_inclusion p1 mono2 i 0 in
          if not to_be_inserted then (
            (* continue with the rest of monomials with the shifted i *)
            zip_sum p1 p2' i)
          else
            (* if new_poly = [] then *)
            (*   p2' *)
            (* if we are at the end of filtered p1 : new_poly *)
            if i = List.length new_poly then 
              (* insert (if necessary) all the other monomials of p2 
               * in new_poly *)
              let i, new_poly = List.fold_left (fun (i,new_poly) m -> 
                let to_be_inserted, new_poly, i = poly_inclusion new_poly m i 0 in
                if to_be_inserted then 
                  (i,new_poly@[m])
                else 
                  (i,new_poly)
              ) (i,new_poly) p2 in
              (* XXX break the rec call here *)
              new_poly
            else 
              (* case where the cursor i is not at the end of new_poly *) 
                try (
                  (* take the i^th monomial of new_poly *)
                  let m1 = List.nth new_poly i in
                  (* compare it with mono2 *)
                  match Mono.compare_deltas (Mono.get_deltas m1) (Mono.get_deltas mono2) with
                  | MonomialBase.Smaller -> 
                      (* continue by only incrementing i *)
                      zip_sum new_poly p2 (i+1)
                  | MonomialBase.Larger -> 
                      (* insert mono2 at cursor and continue 
                       * for next mono2 of p2 *)
                      let new_poly = insert_at mono2 i new_poly in
                      zip_sum new_poly p2' (i+1)
                  | MonomialBase.Equal -> 
                      (* if they are equal just change the scalar *)
                      let scalar = Mono.sum_scalar m1 mono2 in
                      let new_poly = change_scalar_at scalar i new_poly in
                      (* continue with next mono2 *)
                      zip_sum new_poly p2' i
                ) with _ -> (
                    (* in all other cases insert mono2 in new_poly 
                     * and continue *) 
                    let new_poly = insert_at mono2 i new_poly in
                    zip_sum new_poly p2' (i+1))

  (* sum two polynomials *)
  (* - If both lists are empty the result is empty. *)
  (* - If one list is empty, the result will be the other list *)
  (* of polynomials. *)
  (* Otherwise the operation will zip the two lists together *)
  (* and return a new polynomial of sorted monomials. *)
  (* Arguments: *)
  (*     polynomial: Polynomial to add to self *)
  (* Returns: *)
  (*     New, sorted polynomial that is a sum of the *)
  (*     two input polynomials *)
  let sum (p1:t) (p2:t) : t =
    match p1 with
    | [] -> p2
    | _ -> match p2 with
      | [] -> p1
      | _ -> 
    let res = poly_sort (zip_sum p1 p2 0) in
    if res = [] then poly_zero else res

  let equal (p1:t) (p2:t) : bool =
    List.length p1 = List.length p2 &&
    List.for_all2 (fun m1 m2 -> Mono.equal m1 m2) p1 p2

  (* compare two polynomials *)
  (* return 0 if equal, -1 if p1 < p2, 1 otherwise *)
  let compare (p1:t) (p2:t) : int =
    match p1,p2 with
    | [], [] -> 0
    | [], _ -> -1
    | _, [] -> 1
    | h1::_, h2::_ -> Mono.compare h1 h2

  (* insert a polynomial in a list preserving order *)
  let rec poly_insert_sort (p:t) (sorted_polys:t list) : t list =
    if p <> [] then
      match sorted_polys with
      | [] -> [p]
      | h::t -> 
          (* if p < h *)
          if compare p h < 0 then 
            p::h::t
          else h::(poly_insert_sort p t)
    else sorted_polys

  (* 1. We consider the first element — smallest_p *)
  (* 2. If smallest_p is not empty, take the first monomial mono2 and insert the rest_p *)
  (*    at the right position: for this we compare the first *)
  (*    element of rest_p to the first monomial of t (all poly without smallest_p) *)
  (* 3. We rec call. Unless no element is left *)
  let rec sorted_monomials_from_sorted_polys (sorted_polys:t list) : monomial list =
    match sorted_polys with
    | [] -> []
    | smallest_p::t -> 
        match smallest_p with
        | [] -> Printf.eprintf "Should not be empty\n"; assert false
        | mono2::rest_p -> 
            let new_t = poly_insert_sort rest_p t in
            let rec_poly = sorted_monomials_from_sorted_polys new_t in
            (* NOTE: we also perform a inclusion test here to avoid inserting redondant constraints *) 
            let to_be_inserted,rec_poly,_ = poly_inclusion rec_poly mono2 0 0 in
            if to_be_inserted then
              mono2::rec_poly
            else rec_poly

            (* Multiply two polynomials. *)
  (* Here we assume at least self is a sorted polynomial, *)
  (* and the result of this operation will be sorted. *)
  (* Arguments: *)
      (* p1, p2: polynomials to multiply *)
  (* Returns: *)
      (* a new polynomial that is the sorted product *)
      (* of the two input polynomials *)
  let prod (p1:t) (p2:t) : t =
    (*here we compute P1 x P2 for each polynomial, excluding from the *)
    (*result all monomials that have scalar value <> 0 *)
    let products:monomial list list = 
      List.fold_left ( fun all m2 ->
        let more:monomial list = List.fold_left ( fun prods m1 -> 
          let mono:monomial = Mono.prod m1 m2 in
          (* Printf.eprintf "\n%s * %s = %s\n" (Mono.to_str m1) (Mono.to_str m2) (Mono.to_str mono); *)
          if Mono.get_scalar mono <> Mono.smr_zero then
            mono::prods
          else prods
        ) [] p1 in
        if more <> [] then more::all else all
      ) [] p2 in

    (*if table is empty, return zero polynomial *)
    if products = [] then poly_zero else

    (* sort the polynomials *)
    let sorted_polys:t list = List.sort compare products in
    let sorted_polys:t list = List.filter (fun p -> p <> []) sorted_polys in
    (* remove the empty ones *) 
    let res = sorted_monomials_from_sorted_polys sorted_polys in
    if res = [] then poly_zero else res

  let correction (ndg:n_deltagraphs) (p:t) (i,j:reg*reg): (n_deltagraphs*t) =
    List.fold_left (fun (ndg,p') mon ->
      let (ndg,mon') = Mono.correction ndg mon (i,j) in
      (* FIXME mon'::p' or p'::mon' ? *)
      (ndg,mon'::p')
    ) (ndg,[]) p
end

(* module Polynomial_MWP = Make(Monomial_MWP) *)

(* TOMOVE tests for polynomial manipulation *)(* {↓{ *)
(* let test_prod = *)
(*   let z = [Monomial_MWP.cons(Monomial_MWP.smr_zero, [])] in *)
(*   let p = [Monomial_MWP.cons(M, [Delta(1, P.of_int 1); Delta(2, P.of_int 2)])] in *)
(*   let c = Polynomial_MWP.prod p z in *)
(*   assert (Polynomial_MWP.equal c poly_zero); *)
(*   let mono1 = Mono(M, [Delta(3, P.of_int 3)]) in *)
(*   let mono2 = Mono(M, [Delta(1, P.of_int 1); Delta(2, P.of_int 2)]) in *)
(*   let m = [mono1] in *)
(*   let p = [mono2] in *)
(*   let  c = Polynomial_MWP.prod p m in *)
(*   let expected = [Mono(M, [Delta(1, P.of_int 1); Delta(2, P.of_int 2); Delta(3, P.of_int 3)])] in *)
(*   (1* Printf.eprintf "%s\n" (str_polynomial c); *1) *)
(*   (1* Printf.eprintf "%s\n" (str_polynomial expected); *1) *)
(*   assert (Polynomial_MWP.equal c expected) *)

(* let test_sum = *)
(*   let mono1 = Mono.cons(M,[Delta(3, (P.of_int 3))]) in *)
(*   let mono2 = Mono.cons(M,[Delta(1, (P.of_int 1));Delta(2, (P.of_int 2))]) in *)
(*   let m = [mono1] in *)
(*   let p = [mono2] in *)
(*   let res = Polynomial_MWP.sum m p in *)
(*   let res2 = Polynomial_MWP.sum p m in *)
(*   let expected = *) 
(*     [Mono(M, [Delta(1, P.of_int 1); Delta(2, P.of_int 2)]); Mono(M, [Delta(3, P.of_int 3)])] in *)
(*     assert (res = expected); *)
(*     assert (res2 = expected); *)

(*   let mono1 = Mono.cons(M,[Delta(1, (P.of_int 2));Delta(1, (P.of_int 3));Delta(1, (P.of_int 4))]) in *)
(*   let mono2 = Mono.cons(W,[Delta(1, (P.of_int 3));Delta(1, (P.of_int 4));Delta(1, (P.of_int 5))]) in *)
(*   let mono3 = Mono.cons(P,[Delta(1, (P.of_int 3));Delta(1, (P.of_int 4));Delta(1, (P.of_int 6))]) in *)
(*   let m = poly_sort [mono1;mono2;mono3] in *)
(*     (1* m = Polynomial(Polynomial.sort_monomials([mono1, mono2, mono3])) *1) *)
(*   let mono0 = Mono.cons(W,[Delta(1, (P.of_int 3));Delta(1, (P.of_int 4))]) in *)
(*     (1* mono0 = Monomial('w', [(0, 2), (0, 3)]) *1) *)
(*   let p = [mono0] in *)
(*     (1* p = Polynomial([mono0]) *1) *)
(*   let c = Polynomial_MWP.sum p m in *)
(*   let c2 = Polynomial_MWP.sum m p in *)
(*   let expected = [mono0; mono3] in *)
(*   assert (c = expected); *)
(*   assert (c2 = expected) *)

(* let poly_tests = *)
(*   test_prod; *)
(*   test_sum; *)
(*   Printf.eprintf "poly tests OK!\n"(1* }↑} *1) *)
