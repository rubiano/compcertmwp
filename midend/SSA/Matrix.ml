open BinNums
open Maps
open Registers

module type S =
  sig
    type polynomial
    type monomial
    type scalar
    type delta_choice
    type choice
    type t = polynomial PTree.t PTree.t
    type n_deltagraphs

    val poly_zero: polynomial
    val poly_unit: polynomial
    val poly_sum: polynomial -> polynomial -> polynomial
    val poly_prod: polynomial -> polynomial -> polynomial
    val poly_eq: polynomial -> polynomial -> bool

    val get_polynome_at: t -> reg -> reg -> polynomial
    val set_polynome_at: t -> reg -> reg -> polynomial -> t

    val identity_matrix: reg list -> t
    val sum: reg list -> t -> t -> t
    val sum': t -> t -> t
    val prod: reg list -> t -> t -> t
    val loop_correction: n_deltagraphs -> t -> reg list -> (n_deltagraphs*t)
    val str_polynomial: polynomial -> string
    val fusion: n_deltagraphs ref -> int -> n_deltagraphs*bool
    val poly_cons: monomial list -> polynomial
    val mono_cons: scalar * delta_choice list -> monomial
    val delta_cons: (choice*positive) -> delta_choice
    val new_ndg: n_deltagraphs
  end

module Make(Choices: Set.S)(SemR: SemiRing.S):(S with type scalar = SemR.elt and type choice = Choices.elt) = struct
  module Polynome = Polynomial.Make(Choices)(SemR)
  type 'a matrix = 'a PTree.t PTree.t

  (* TODO rewrite ↓ *)
  type polynomial = Polynome.t
  type t = polynomial matrix
  let poly_zero = Polynome.zero
  let poly_unit = Polynome.one
  let poly_sum = Polynome.sum
  let poly_prod = Polynome.prod
  let poly_eq = Polynome.equal
  let fusion = Polynome.fusion
  type n_deltagraphs = Polynome.n_deltagraphs
  type monomial = Polynome.monomial

  type scalar = SemR.elt
  type choice = Choices.elt

  type delta_choice = Polynome.delta_choice
  let poly_cons = Polynome.cons
  let mono_cons = Polynome.mono_cons
  let delta_cons = Polynome.delta_cons
  let new_ndg = Polynome.new_ndg

  (* Get the polynome at the i j coordinates *)
  (* Arguments: *)
  (*     matrix: the matrix where to get that poly *)
  (*     i,j: coordinates *)
  (* Returns: *)
  (*     The poly at i j (if not set then poly_zero). *)
  let get_polynome_at (matrix:polynomial matrix) (i:reg) (j:reg):polynomial =
      match (PTree.get i matrix) with
      | None -> failwith ("get_polynome_at: unknown node")
      | Some line -> 
          match (PTree.get j line) with
          (* NOTE if not set then it's poly_zero
           * do not need to init matrices anymore *)
          | None -> poly_zero
          | Some p -> p

  let str_polynomial = Polynome.to_str

  (* Set the polynome at the i j coordinates *)
  (* Arguments: *)
  (*     matrix: the matrix where to set the poly *)
  (*     i,j: coordinates *)
  (*     p: polynomial *)
  (* Returns: *)
  (*     updated matrix with p at i j. *)
  let set_polynome_at (matrix:polynomial matrix) (i:reg) (j:reg) (p:polynomial): polynomial matrix = 
      match (PTree.get i matrix) with
      | None -> failwith ("set_polynome_at: unknown node")
      | Some line -> 
          PTree.set i (PTree.set j p line) matrix

  (* Create identity matrix of specified size. *)
  (* Arguments: *)
  (*     vars: vars of the matrix *)
  (* Returns: *)
  (*     New identity matrix. *)
  let identity_matrix (vars:reg list) : polynomial matrix =
    List.fold_left (fun mat vi ->
      let line = List.fold_left (fun line vj ->
        if vi = vj then PTree.set vj poly_unit line else line
      ) PTree.empty vars in
      PTree.set vi line mat
    ) PTree.empty vars

  (* Compute the sum of two matrices. *)
  (* Arguments: *)
  (*     m1: first matrix. *)
  (*     m2: second matrix. *)
  (* Returns: *)
  (*     new matrix that represents the sum of the two inputs. *)
  let mat_sum' (m1:polynomial matrix) (m2:polynomial matrix) : polynomial matrix =
    let get_m2 i j = get_polynome_at m2 i j in
    PTree.map (fun i line -> PTree.map (fun j p -> poly_sum p (get_m2 i j)) line) m1

  let sum' = mat_sum'

  (*Compute the sum of two matrices. *)
  (* Arguments: *)
  (*     m1: first matrix. *)
  (*     m2: second matrix. *)
  (* Returns: *)
  (*     new matrix that represents the sum of the two inputs. *)
  let mat_sum (vars: reg list) (m1:polynomial matrix) (m2:polynomial matrix) : polynomial matrix = 
    List.fold_left (fun mat i ->
      let line = List.fold_left (fun line j -> 
        let total = poly_sum (get_polynome_at m1 i j) (get_polynome_at m2 i j) in
        (* Printf.printf "[%s;%s] %s = %s + %s\n" (str_reg i) (str_reg j) (str_polynomial total) (str_polynomial (get_polynome_at m1 i j)) (str_polynomial (get_polynome_at m2 i j)); *)
        PTree.set j total line
      ) PTree.empty vars in
      PTree.set i line mat
    ) PTree.empty vars

  let sum = mat_sum

  (* Compute the product of two polynomial matrices. *)
  (* Arguments: *)
  (*     m1: first polynomial matrix. *)
  (*     m2: second polynomial matrix. *)
  (* Returns: *)
  (*     new matrix that represents the product of the two inputs. *)
  let matrix_prod (vars: reg list) (m1: polynomial matrix) (m2: polynomial matrix) : polynomial matrix =
    List.fold_left (fun mat i ->
      let line = List.fold_left (fun line j -> 
        (* Printf.eprintf "[%s;%s] = " (str_reg i) (str_reg j); *)
        let total = List.fold_left (fun total k -> 
          (* Printf.eprintf "(%s * %s) + " (str_polynomial (get_polynome_at m1 i k)) (str_polynomial (get_polynome_at m2 k j)); *)
          poly_sum total
          (poly_prod (get_polynome_at m1 i k) (get_polynome_at m2 k j))
        ) poly_zero vars in
        (* Printf.eprintf "= %s\n" (str_polynomial total); *)
        PTree.set j total line
      ) PTree.empty vars in
      PTree.set i line mat
    ) PTree.empty vars

  let prod = matrix_prod

  let loop_correction (ndg:n_deltagraphs) (matrix:t) (vars:reg list): (n_deltagraphs*t) =  
    List.fold_left ( fun (ndg,mat) i ->
      List.fold_left ( fun (ndg,mat') j -> 
        let p = (get_polynome_at matrix i j) in
        let (ndg,new_p) = Polynome.correction ndg p (i,j) in
        (ndg,(set_polynome_at mat' i j new_p))
      ) (ndg,mat) vars
    ) (ndg,matrix) vars
end

  (* tests TOMOVE *)(* {↓{ *)
(* let test_matrix_prod = *) 
(*   let p1 = [(Mono (P,[Delta(1, (P.of_int 2))]));(Mono (W,[Delta(2, (P.of_int 2))]))] in *)
(*   let p2 = [(Mono (M,[Delta(1, (P.of_int 2))]));(Mono (W,[Delta(2, (P.of_int 2))]))] in *)
(*   let p3 = [(Mono (M,[Delta(1, (P.of_int 3))]));(Mono (W,[Delta(2, (P.of_int 3))]))] in *)
(*   let p4 = [(Mono (P,[Delta(1, (P.of_int 3))]));(Mono (W,[Delta(2, (P.of_int 3))]))] in *)

(*   let regs = List.map (P.of_int) [1;2;3] in *)
(*   let r1 = List.nth regs 0 in *)
(*   let r2 = List.nth regs 1 in *)
(*   let r3 = List.nth regs 2 in *)
(*   let o = poly_zero in *)
(*   let m = poly_unit in *)

(*   let mat_a_1,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [m;o;p1] in *)

(*   let mat_a_2,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [o;m;p2] in *)

(*   let mat_a_3,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [o;o;o] in *)

(*   let (mat_a,_:polynomial matrix * int) = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [mat_a_1;mat_a_2;mat_a_3] in *)

(*   let mat_b_1,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [m;p3;o] in *)

(*   let mat_b_2,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [o;m;o] in *)

(*   let mat_b_3,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [o;p4;m] in *)

(*   let (mat_b,_:polynomial matrix * int) = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [mat_b_1;mat_b_2;mat_b_3] in *)



(*     (1* #          m p3 o *1) *)
(*     (1* #          o m  o *1) *)
(*     (1* #          o p4 m *1) *)
(*     (1* # m o p1 *1) *)
(*     (1* # o m p2 *1) *)
(*     (1* # o o o *1) *)
(*     (1* mat_a = [[m, o, p1], [o, m, p2], [o, o, o]] *1) *)
(*     (1* mat_b = [[m, p3, o], [o, m, o], [o, p4, m]] *1) *)
(*     (1* result = matrix_prod(mat_a, mat_b) *1) *)

(*   let result = matrix_prod regs mat_a mat_b in *)

(*   let p_11 = get_polynome_at result r1 r1 in *)
(*   let p_11' = poly_sum (poly_sum (poly_prod m m) (poly_prod o o)) (poly_prod p1 o) in *)
(*   assert (poly_eq p_11 p_11'); *)
(*         (1* assert result[0][0] == (m * m) + (o * o) + (p1 * o) *1) *)

(*   let p_12 = get_polynome_at result r1 r2 in *)
(*   let p_12' = poly_sum (poly_sum (poly_prod m p3) (poly_prod o m)) (poly_prod p1 p4) in *)
(*   assert (poly_eq p_12 p_12'); *)
(*         (1* assert result[0][1] == (m * p3) + (o * m) + (p1 * p4) *1) *)

(*   let p_13 = get_polynome_at result r1 r3 in *)
(*   let p_13' = poly_sum (poly_sum (poly_prod m o) (poly_prod o o)) (poly_prod p1 m) in *)
(*   assert (poly_eq p_13 p_13'); *)
(*         (1* assert result[0][2] == (m * o) + (o * o) + (p1 * m) *1) *)

(*   let p_21 = get_polynome_at result r2 r1 in *)
(*   let p_21' = poly_sum (poly_sum (poly_prod o m) (poly_prod m o)) (poly_prod p2 o) in *)
(*   assert (poly_eq p_21 p_21'); *)
(*         (1* assert result[1][0] == (o * m) + (m * o) + (p2 * o) *1) *)

(*   let p_22 = get_polynome_at result r2 r2 in *)
(*   let p_22' = poly_sum (poly_sum (poly_prod o p3) (poly_prod m m)) (poly_prod p2 p4) in *)
(*   assert (poly_eq p_22 p_22'); *)
(*         (1* assert result[1][1] == (o * p3) + (m * m) + (p2 * p4) *1) *)

(*   let p_23 = get_polynome_at result r2 r3 in *)
(*   let p_23' = poly_sum (poly_sum (poly_prod o o) (poly_prod m o)) (poly_prod p2 m) in *)
(*   assert (poly_eq p_23 p_23'); *)
(*         (1* assert result[1][2] == (o * o) + (m * o) + (p2 * m) *1) *)

(*   let p_31 = get_polynome_at result r3 r1 in *)
(*   let p_31' = poly_sum (poly_sum (poly_prod o m) (poly_prod o o)) (poly_prod o o) in *)
(*   assert (poly_eq p_31 p_31'); *)
(*         (1* assert result[2][0] == (o * m) + (o * o) + (o * o) *1) *)

(*   let p_32 = get_polynome_at result r3 r2 in *)
(*   let p_32' = poly_sum (poly_sum (poly_prod o p3) (poly_prod o m)) (poly_prod o p4) in *)
(*   assert (poly_eq p_32 p_32'); *)
(*         (1* assert result[2][1] == (o * p3) + (o * m) + (o * p4) *1) *)

(*   let p_33 = get_polynome_at result r3 r3 in *)
(*   let p_33' = poly_sum (poly_sum (poly_prod o o) (poly_prod o o)) (poly_prod o m) in *)
(*   assert (poly_eq p_33 p_33') *)
(*         (1* assert result[2][2] == (o * o) + (o * o) + (o * m) *1) *)
(*   (1* Printf.eprintf " %s\n=\n %s\n" (str_polynomial p_21) (str_polynomial p_21'); *1) *)

(* let test_matrix_sum = *) 
(*   let expected = [(Mono (M,[Delta(1, (P.of_int 1))]));(Mono (W,[Delta(2, (P.of_int 2))]))] in *)

(*   let p1 = [(Mono (M,[Delta(1, (P.of_int 1))]))] in *)
(*   let p2 = [(Mono (W,[Delta(2, (P.of_int 2))]))] in *)

(*   let regs = List.map (P.of_int) [1;2] in *)
(*   let r1 = List.nth regs 0 in *)
(*   let r2 = List.nth regs 1 in *)

(*   let mat_a_1,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [p1;p1] in *)

(*   let (mat_a,_:polynomial matrix * int) = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [mat_a_1;mat_a_1] in *)

(*   let mat_b_1,_ = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [p2;p2] in *)

(*   let (mat_b,_:polynomial matrix * int) = List.fold_left *) 
(*     (fun (a,n) p -> (PTree.set (P.of_int n) p a, n+1)) *)
(*     (PTree.empty,1) [mat_b_1;mat_b_1] in *)

(*   let result = mat_sum regs mat_a mat_b in *)

(*   let p_11 = get_polynome_at result r1 r1 in *)
(*   let p_11' = expected in *)
(*   assert (poly_eq p_11 p_11'); *)
(*     (1* assert result[0][0] == expected *1) *)

(*   let p_12 = get_polynome_at result r1 r2 in *)
(*   let p_12' = expected in *)
(*   assert (poly_eq p_12 p_12'); *)
(*     (1* assert result[0][1] == expected *1) *)

(*   let p_21 = get_polynome_at result r2 r1 in *)
(*   let p_21' = expected in *)
(*   assert (poly_eq p_21 p_21'); *)
(*     (1* assert result[1][0] == expected *1) *)

(*   let p_22 = get_polynome_at result r2 r2 in *)
(*   let p_22' = expected in *)
(*   assert (poly_eq p_22 p_22') *)
(*     (1* assert result[1][1] == expected *1) *)

(* let test_matrix = *)
(*   test_matrix_prod; *)
(*   test_matrix_sum; *)
(*   Printf.eprintf "Matrix tests OK !\n"(1* }↑} *1) *)
