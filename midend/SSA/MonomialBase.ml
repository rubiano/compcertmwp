open BinNums
open Camlcoq

type check = Smaller | Equal | Larger
type inclusion = INCLUDED | EMPTY | CONTAINS

module type S =
  sig
    type t
    type scalar
    type choice (* orderedtype with compare*)
    type 'a delta = Delta of 'a * positive
    type delta_choice = choice delta (* delta choice : choice * positive *)
    val zero: t
    val one: t

    val get_deltas: t -> delta_choice list
    val get_scalar: t -> scalar
    val get_product: t -> scalar * delta_choice list
    val get_index: 'a delta -> positive
    val isNull: t -> bool
    val cons: scalar * delta_choice list -> t
    val delta_cons: choice*positive -> delta_choice

    val compare: t -> t -> int
    val equal: t -> t -> bool
    val to_str: t -> string
  end

module Base(Choices: Set.S) (SemR: SemiRing.S):(S with type scalar = SemR.elt and type choice = Choices.elt) = struct
  type scalar = SemR.elt

  type choice = Choices.elt
  type 'a delta = Delta of 'a * positive
  type delta_choice = choice delta
  type t = Mono of SemR.elt * delta_choice list

  (* let makeDelta (c,p:choice*positive) = Delta(c,p) *)

  let zero = Mono (SemR.zero, [])
  let one = Mono (SemR.one, [])
  (* let smr_zero = SemR.zero *)

  let get_deltas = function Mono (_,deltas) -> deltas
  let get_scalar:t->SemR.elt = function Mono (s,_) -> s
  let get_product:t->(SemR.elt*delta_choice list) = function Mono (s,l) -> (s,l)
  let get_index:'a delta->positive = function Delta(_,i) -> i

  let to_str (m:t) : string =
    let (v, deltas) = get_product m in
    List.fold_left (fun s (Delta (i,j)) -> 
      (* TODO having to_string function for choices elt *)
      (* Printf.sprintf "%s.(%s,%ld)" s (to_str i) (P.to_int32 j) *)
      Printf.sprintf "%s.(_,%ld)" s (P.to_int32 j)
      ) (SemR.to_string v) deltas


  let isNull (m:t) = SemR.isNull (get_scalar m)
  let cons ((sca,dl):(scalar*delta_choice list)) : t = Mono(sca,dl) 
  let delta_cons (c,i:choice*positive) : delta_choice = Delta(c,i)

  (* Compare two list of deltas 
   * Arguments:
     * deltas1, deltas2 : list of deltas to compare
   * Return: 
     * Equal if equality
     * Larger if deltas1 > deltas2
     * Smaller if deltas1 < deltas2
     *)
  let rec compare_deltas (deltas1:delta_choice list) (deltas2:delta_choice list) : check =
    match deltas1, deltas2 with
    | [], [] -> Equal
    | d::t, [] -> Larger
    | d1::t1, d2::t2 -> if d1 = d2 then compare_deltas t1 t2 else
      let Delta (i,j) = d1 in let Delta (m,n) = d2 in
      (*--------------------------↓ FIXME does it work with compare ? *)
      if (j<n) || (j == n && i < m) then Smaller else Larger
      (* we may prefer using compare *)
      (* if (j<n) || (j == n && (Choices.compare i m < 0)) then Smaller else Larger *)
    | ([], _) -> Smaller

  (* Compare two monomials 
   * If the scalars are equals, compare the deltas
   * Arguments:
     * m1, m2 : monomials to compare
   * Return: 
     * 0 if equal
     * 1 if m1 > m2
     * -1 if m1 < m2
     *)
  let compare (m1:t) (m2:t) : int =
    if get_scalar m1 = get_scalar m2 then
      match compare_deltas (get_deltas m1) (get_deltas m2) with
      | Equal -> 0
      | Larger -> 1
      | Smaller -> -1
    else SemR.compare (get_scalar m1) (get_scalar m2)

  let equal (m1:t) (m2:t) = (compare m1 m2 = 0)
end

