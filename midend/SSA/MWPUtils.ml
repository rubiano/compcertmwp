open Camlcoq

let rec insert_at x n = function
  | [] -> []
  | h::t as l -> if n = 0 then x::l else h::(insert_at x (n-1) t)

let rec split_at n acc l =
  if n = 0 then (List.rev acc, l) else
  match l with
  | [] -> (List.rev acc, [])
  | h :: t -> split_at (n-1) (h::acc) t

let rec nth_truncate (n:int) (l:'a list) : ('a list) =
  match n,l with
  | 0, _ -> l
  | _, [] -> l
  | n,h::t -> nth_truncate (n-1) t

let rec remove_first (e:'a) (l:'a list) : 'a list =
  match l with
  | [] -> []
  | h::t -> if h = e then t else h::remove_first e t

let str_reg r =
  Printf.sprintf "x%ld" (P.to_int32 r)
